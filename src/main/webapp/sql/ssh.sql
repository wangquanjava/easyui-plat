/*
SQLyog v10.2 
MySQL - 5.1.40-community : Database - jeecg2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`jeecg2` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `jeecg2`;

/*Table structure for table `persistent_logins` */

DROP TABLE IF EXISTS `persistent_logins`;

CREATE TABLE `persistent_logins` (
  `username` varchar(100) NOT NULL,
  `series` varchar(100) DEFAULT NULL,
  `token` varchar(100) DEFAULT NULL,
  `last_used` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `persistent_logins` */

/*Table structure for table `sys_authorities` */

DROP TABLE IF EXISTS `sys_authorities`;

CREATE TABLE `sys_authorities` (
  `ID` bigint(13) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `AUTHORITY_ID` varchar(32) NOT NULL COMMENT '认证id',
  `AUTHORITY_NAME` varchar(40) DEFAULT NULL COMMENT '认证名称',
  `AUTHORITY_DESC` varchar(100) DEFAULT NULL COMMENT '认证描述',
  `ENABLED` bigint(1) DEFAULT NULL COMMENT '是否启用',
  `ISSYS` bigint(1) DEFAULT NULL COMMENT '是否管理员',
  `MODULE` varchar(4) DEFAULT NULL COMMENT '模块',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `sys_authorities` */

insert  into `sys_authorities`(`ID`,`AUTHORITY_ID`,`AUTHORITY_NAME`,`AUTHORITY_DESC`,`ENABLED`,`ISSYS`,`MODULE`) values (7,'AUTH_TEST','普通用户','普通用户',1,1,'01'),(8,'AUTH_ADMIN','管理员权限','管理员权限',1,1,'');

/*Table structure for table `sys_authorities_resources` */

DROP TABLE IF EXISTS `sys_authorities_resources`;

CREATE TABLE `sys_authorities_resources` (
  `ID` bigint(13) NOT NULL AUTO_INCREMENT,
  `AUTHORITY_ID` varchar(32) DEFAULT NULL,
  `RESOURCE_ID` varchar(32) DEFAULT NULL,
  `ENABLED` bigint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

/*Data for the table `sys_authorities_resources` */

insert  into `sys_authorities_resources`(`ID`,`AUTHORITY_ID`,`RESOURCE_ID`,`ENABLED`) values (46,'AUTH_TEST','100100',1),(47,'AUTH_TEST','100',1),(71,'AUTH_ADMIN','100100',1),(72,'AUTH_ADMIN','100101',1),(73,'AUTH_ADMIN','100102',1),(74,'AUTH_ADMIN','100103',1),(75,'AUTH_ADMIN','100106',1),(76,'AUTH_ADMIN','100',1);

/*Table structure for table `sys_resources` */

DROP TABLE IF EXISTS `sys_resources`;

CREATE TABLE `sys_resources` (
  `ID` bigint(13) NOT NULL AUTO_INCREMENT COMMENT '表pk',
  `RESOURCE_ID` varchar(32) NOT NULL COMMENT '菜单id',
  `RESOURCE_NAME` varchar(100) DEFAULT NULL COMMENT '名称',
  `RESOURCE_DESC` varchar(100) DEFAULT NULL COMMENT '说明',
  `RESOURCE_TYPE` varchar(40) DEFAULT NULL COMMENT '类型',
  `RESOURCE_STRING` varchar(200) DEFAULT NULL COMMENT '链接',
  `PRIORITY` bigint(1) DEFAULT NULL,
  `ENABLED` bigint(1) DEFAULT NULL COMMENT '是否启用',
  `ISSYS` bigint(1) DEFAULT NULL,
  `MODULE` varchar(4) DEFAULT NULL,
  `pid` varchar(32) DEFAULT NULL COMMENT '父id',
  `level` bigint(3) DEFAULT NULL COMMENT '级别',
  `RSORT` bigint(11) DEFAULT NULL COMMENT '排序',
  `is_Leaf` varchar(4) DEFAULT NULL COMMENT '是否是叶子',
  `icon` varchar(30) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `sys_resources` */

insert  into `sys_resources`(`ID`,`RESOURCE_ID`,`RESOURCE_NAME`,`RESOURCE_DESC`,`RESOURCE_TYPE`,`RESOURCE_STRING`,`PRIORITY`,`ENABLED`,`ISSYS`,`MODULE`,`pid`,`level`,`RSORT`,`is_Leaf`,`icon`) values (28,'100','系统配置','系统配置','1','/',NULL,1,NULL,NULL,'',1,1,'0',NULL),(29,'100100','用户管理','用户管理','1','/userController.do?toListPage&pathroot=user&page=list',NULL,1,1,NULL,'100',2,1,'1',NULL),(30,'100101','角色管理','角色管理','1','/rolesController.do?toListPage&pathroot=roles&page=list',NULL,1,1,NULL,'100',2,1,'1',NULL),(31,'100102','权限管理','权限管理','1','/authorsController.do?toListPage&pathroot=authors&page=list',NULL,1,1,NULL,'100',2,1,'1',NULL),(32,'100103','菜单管理','菜单管理','1','/resController.do?toListPage&pathroot=res&page=list',NULL,1,1,NULL,'100',2,1,'1',NULL),(33,'101','系统监控','系统监控','1','/',NULL,1,NULL,NULL,'',1,1,'0',NULL),(34,'101100','日志','日志','1','1',NULL,1,NULL,NULL,'101',2,1,'1',NULL),(35,'100104','菜单一','菜单一','1','1',NULL,1,NULL,NULL,'100',2,1,'1',NULL),(38,'100106','字典管理','字典管理','1','/dictionaryController.do?toListPage&pathroot=dictionary&page=list',NULL,1,NULL,NULL,'100',2,1,'1',NULL);

/*Table structure for table `sys_roles` */

DROP TABLE IF EXISTS `sys_roles`;

CREATE TABLE `sys_roles` (
  `ID` bigint(13) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `ROLE_ID` varchar(32) NOT NULL COMMENT '权限ID',
  `ROLE_NAME` varchar(40) DEFAULT NULL COMMENT '权限名称',
  `ROLE_DESC` varchar(100) DEFAULT NULL COMMENT '权限描述',
  `ENABLED` bigint(1) DEFAULT NULL COMMENT '是否启用',
  `ISSYS` bigint(1) DEFAULT NULL COMMENT '是否是管理员',
  `MODULE` varchar(4) DEFAULT NULL COMMENT '模块名称',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `sys_roles` */

insert  into `sys_roles`(`ID`,`ROLE_ID`,`ROLE_NAME`,`ROLE_DESC`,`ENABLED`,`ISSYS`,`MODULE`) values (9,'ROLE_TEST','测试用户','权限组描述',1,1,'001'),(10,'ROLE_ADMIN','管理员角色','管理员角色',1,1,'');

/*Table structure for table `sys_roles_authorities` */

DROP TABLE IF EXISTS `sys_roles_authorities`;

CREATE TABLE `sys_roles_authorities` (
  `ID` bigint(13) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` varchar(32) DEFAULT NULL,
  `AUTHORITY_ID` varchar(32) DEFAULT NULL,
  `ENABLED` bigint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

/*Data for the table `sys_roles_authorities` */

insert  into `sys_roles_authorities`(`ID`,`ROLE_ID`,`AUTHORITY_ID`,`ENABLED`) values (28,'ROLE_ADMIN','AUTH_ADMIN',1),(31,'ROLE_TEST','AUTH_TEST',1);

/*Table structure for table `sys_type` */

DROP TABLE IF EXISTS `sys_type`;

CREATE TABLE `sys_type` (
  `id` varchar(255) NOT NULL,
  `typecode` varchar(50) DEFAULT NULL,
  `typename` varchar(50) DEFAULT NULL,
  `typepid` varchar(255) DEFAULT NULL,
  `typegroupid` varchar(255) DEFAULT NULL,
  `typeid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7D26E9B1C6BEA51C` (`typepid`),
  KEY `FK7D26E9B15395FBB4` (`typegroupid`),
  CONSTRAINT `FK7D26E9B15395FBB4` FOREIGN KEY (`typegroupid`) REFERENCES `sys_type_group` (`id`),
  CONSTRAINT `FK7D26E9B1C6BEA51C` FOREIGN KEY (`typepid`) REFERENCES `sys_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_type` */

insert  into `sys_type`(`id`,`typecode`,`typename`,`typepid`,`typegroupid`,`typeid`) values ('402880fb43a590a30143a59119df004d','2','菜单图标',NULL,'402880fb43a590a30143a59119aa0043',NULL),('402880fb43a590a30143a59119e2004e','1','系统图标',NULL,'402880fb43a590a30143a59119aa0043',NULL),('402880fb43a590a30143a59119e4004f','files','附件',NULL,'402880fb43a590a30143a59119bd004b',NULL),('402880fb43a590a30143a59119e60050','1','优质订单',NULL,'402880fb43a590a30143a59119af0044',NULL),('402880fb43a590a30143a59119e70051','2','普通订单',NULL,'402880fb43a590a30143a59119af0044',NULL),('402880fb43a590a30143a59119e90052','1','签约客户',NULL,'402880fb43a590a30143a59119b10045',NULL),('402880fb43a590a30143a59119eb0053','2','普通客户',NULL,'402880fb43a590a30143a59119b10045',NULL),('402880fb43a590a30143a59119ec0054','1','特殊服务',NULL,'402880fb43a590a30143a59119b30046',NULL),('402880fb43a590a30143a59119ee0055','2','普通服务',NULL,'402880fb43a590a30143a59119b30046',NULL),('402880fb43a590a30143a59119f00056','single','单条件查询',NULL,'402880fb43a590a30143a59119b40047',NULL),('402880fb43a590a30143a59119f10057','group','范围查询',NULL,'402880fb43a590a30143a59119b40047',NULL),('402880fb43a590a30143a59119f20058','Y','是',NULL,'402880fb43a590a30143a59119b60048',NULL),('402880fb43a590a30143a59119f40059','N','否',NULL,'402880fb43a590a30143a59119b60048',NULL),('402880fb43a590a30143a59119f6005a','Integer','Integer',NULL,'402880fb43a590a30143a59119b80049',NULL),('402880fb43a590a30143a59119f7005b','Date','Date',NULL,'402880fb43a590a30143a59119b80049',NULL),('402880fb43a590a30143a59119f9005c','String','String',NULL,'402880fb43a590a30143a59119b80049',NULL),('402880fb43a590a30143a59119fa005d','Long','Long',NULL,'402880fb43a590a30143a59119b80049',NULL),('402880fb43a590a30143a59119fb005e','act','工作流引擎表',NULL,'402880fb43a590a30143a59119ba004a',NULL),('402880fb43a590a30143a59119fd005f','t_s','系统基础表',NULL,'402880fb43a590a30143a59119ba004a',NULL),('402880fb43a590a30143a59119fe0060','t_b','业务表',NULL,'402880fb43a590a30143a59119ba004a',NULL),('402880fb43a590a30143a5911a000061','t_p','自定义引擎表',NULL,'402880fb43a590a30143a59119ba004a',NULL),('402880fb43a590a30143a5911a010062','news','新闻',NULL,'402880fb43a590a30143a59119bd004b',NULL),('402880fb43a590a30143a5911a030063','0','男性',NULL,'402880fb43a590a30143a59119c7004c',NULL),('402880fb43a590a30143a5911a040064','1','女性',NULL,'402880fb43a590a30143a59119c7004c',NULL),('8a80808243a5c23d0143a5db2ff0000a','C','C',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5db550b000c','D','D',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5db8341000e','E','E',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5dbb1cd0010','F','F',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5df9c1a0013','G','G',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5dfc5400015','H','H',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5e0027a0017','I','I',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5e0308e0019','J','J',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5e05b97001b','K','K',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL),('8a80808243a5c23d0143a5e0823f001d','L','L',NULL,'8a80808243a5c23d0143a5dad58d0008',NULL);

/*Table structure for table `sys_type_group` */

DROP TABLE IF EXISTS `sys_type_group`;

CREATE TABLE `sys_type_group` (
  `id` varchar(255) NOT NULL,
  `typegroupcode` varchar(50) DEFAULT NULL,
  `typegroupname` varchar(50) DEFAULT NULL,
  `typegroupid` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_type_group` */

insert  into `sys_type_group`(`id`,`typegroupcode`,`typegroupname`,`typegroupid`) values ('402880fb43a590a30143a59119aa0043','icontype','图标类型',NULL),('402880fb43a590a30143a59119af0044','order','订单类型',NULL),('402880fb43a590a30143a59119b10045','custom','客户类型',NULL),('402880fb43a590a30143a59119b30046','service','服务项目类型',NULL),('402880fb43a590a30143a59119b40047','searchmode','查询模式',NULL),('402880fb43a590a30143a59119b60048','yesorno','逻辑条件',NULL),('402880fb43a590a30143a59119b80049','fieldtype','字段类型',NULL),('402880fb43a590a30143a59119ba004a','database','数据表',NULL),('402880fb43a590a30143a59119bd004b','fieltype','文档分类',NULL),('402880fb43a590a30143a59119c7004c','sex','性别类',NULL),('8a80808243a5c23d0143a5dad58d0008','pos','所在盘符',NULL);

/*Table structure for table `sys_type_group_t` */

DROP TABLE IF EXISTS `sys_type_group_t`;

CREATE TABLE `sys_type_group_t` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `typegroupcode` varchar(50) DEFAULT NULL,
  `typegroupname` varchar(50) DEFAULT NULL,
  `typegroupid` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `sys_type_group_t` */

insert  into `sys_type_group_t`(`id`,`typegroupcode`,`typegroupname`,`typegroupid`) values (1,'sex','性别',1),(10,'isLeaf','是否是叶子结点',2),(11,'is_user','是否启用',3),(12,'is_user_e','是否启用',4);

/*Table structure for table `sys_type_t` */

DROP TABLE IF EXISTS `sys_type_t`;

CREATE TABLE `sys_type_t` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `typecode` varchar(50) DEFAULT NULL,
  `typename` varchar(50) DEFAULT NULL,
  `typepid` varchar(255) DEFAULT NULL,
  `typegroupid` varchar(255) DEFAULT NULL,
  `typeid` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `sys_type_t` */

insert  into `sys_type_t`(`id`,`typecode`,`typename`,`typepid`,`typegroupid`,`typeid`) values (1,'0','男',NULL,'1',1),(2,'1','女',NULL,'1',2),(16,'1','是',NULL,'2',3),(18,'2','否',NULL,'2',4),(19,'true','是',NULL,'3',5),(20,'false','否',NULL,'3',6),(21,'1','是',NULL,'4',7),(22,'0','否',NULL,'4',8);

/*Table structure for table `sys_users` */

DROP TABLE IF EXISTS `sys_users`;

CREATE TABLE `sys_users` (
  `ID` bigint(13) NOT NULL AUTO_INCREMENT,
  `USER_ID` varchar(32) NOT NULL,
  `USER_ACCOUNT` varchar(30) DEFAULT NULL,
  `USER_NAME` varchar(40) DEFAULT NULL,
  `USER_PASSWORD` varchar(100) DEFAULT NULL,
  `USER_DESC` varchar(100) DEFAULT NULL,
  `ENABLED` bigint(1) DEFAULT NULL,
  `ISSYS` bigint(1) DEFAULT NULL,
  `USER_DEPT` varchar(20) DEFAULT NULL,
  `USER_DUTY` varchar(10) DEFAULT NULL,
  `SUB_SYSTEM` varchar(30) DEFAULT NULL,
  `ROLE_NAMES` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `sys_users` */

insert  into `sys_users`(`ID`,`USER_ID`,`USER_ACCOUNT`,`USER_NAME`,`USER_PASSWORD`,`USER_DESC`,`ENABLED`,`ISSYS`,`USER_DEPT`,`USER_DUTY`,`SUB_SYSTEM`,`ROLE_NAMES`) values (6,'admin','admin','系统管理员','c4ca4238a0b923820dcc509a6f75849b','超级系统管理员',NULL,NULL,NULL,NULL,NULL,'管理员角色'),(7,'user','user','普通用户','c4ca4238a0b923820dcc509a6f75849b','普通用户',NULL,NULL,NULL,NULL,NULL,'      					测试用户\r\n      			  ');

/*Table structure for table `sys_users_roles` */

DROP TABLE IF EXISTS `sys_users_roles`;

CREATE TABLE `sys_users_roles` (
  `ID` bigint(13) NOT NULL AUTO_INCREMENT,
  `USER_ID` varchar(50) DEFAULT NULL,
  `ROLE_ID` varchar(50) DEFAULT NULL,
  `ENABLED` bigint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

/*Data for the table `sys_users_roles` */

insert  into `sys_users_roles`(`ID`,`USER_ID`,`ROLE_ID`,`ENABLED`) values (45,'user','ROLE_TEST',1),(49,'admin','ROLE_ADMIN',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

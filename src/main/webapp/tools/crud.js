
$.fn.panel.defaults.onBeforeDestroy = function() {/* tab关闭时回收内存 */
	var frame = $('iframe', this);
	try {
		if (frame.length > 0) {
			frame[0].contentWindow.document.write('');
			frame[0].contentWindow.close();
			frame.remove();
			if ($.browser.msie) {
				CollectGarbage();
			}
		} else {
			$(this).find('.combo-f').each(function() {
				var panel = $(this).data().combo.panel;
				panel.panel('destroy');
			});
		}
	} catch (e) {
	}
};
$.parser.onComplete = function() {/* 页面所有easyui组件渲染成功后，隐藏等待信息 */
	if ($.browser.msie && $.browser.version < 7) {/* 解决IE6的PNG背景不透明BUG */
	}
	window.setTimeout(function() {
		window.top.$.messager.progress('close');
	}, 200);
};

/**
 * 增删改工具栏
 */
window.onerror = function() {
	return true;
};
var iframe;// iframe操作对象
var win;//窗口对象
var gridname="";//操作datagrid对象名称
var windowapi = frameElement.api, W = windowapi.opener;//内容页中调用窗口实例对象接口

jQuery(function($) {

	$.messager.defaults = {
		ok : "确定",
		cancel : "取消"
	};

	/**
	 * 如果页面是详细查看页面，无效化所有表单元素，只能进行查看,隐藏相关的验证提示
	 */
	if(location.href.indexOf("load=detail")!=-1){
		$(":input").attr("disabled","true");
		$(":input").attr("style","border:0;border-bottom:1 solid black;background:white;");
		$(".Validform_checktip").hide();
	}

});

/**
 * 添加事件打开窗口
 * @param title 编辑框标题
 * @param addurl//目标页面地址
 */
function add(title,addurl,gname,width,height) {
	gridname=gname;
	createwindow(title, addurl,width,height);
}

/**
 * 更新事件打开窗口
 * @param title 编辑框标题
 * @param addurl//目标页面地址
 * @param id//主键字段
 */
function update(title,url, id,width,height) {
	gridname=id;
	var rowsData = $('#'+id).datagrid('getSelections');
	if (!rowsData || rowsData.length==0) {
		tip('请选择编辑项目');
		return;
	}
	if (rowsData.length>1) {
		tip('请选择一条记录再编辑');
		return;
	}
	
	url += '&id='+rowsData[0].id;
	createwindow(title,url,width,height);
}

/**
 * 更新事件打开窗口
 * @param title 编辑框标题
 * @param addurl//目标页面地址
 * @param id//主键字段
 */
function updatetask(title,url, id,width,height) {
	gridname=id;
//	var rowsData = $('#'+id).datagrid('getSelections');
//	if (!rowsData || rowsData.length==0) {
//		tip('请选择要办理的流程!');
//		return;
//	}
//	if (rowsData.length>1) {
//		tip('请选择一条记录再办理!');
//		return;
//	}
//	url += '&id='+rowsData[0].id;
	createtaskwindow(title,url,width,height);
}

/**
 * 查看详细事件打开窗口
 * @param title 查看框标题
 * @param addurl//目标页面地址
 * @param id//主键字段
 */
function detail(title,url, id,width,height) {
	var rowsData = $('#'+id).datagrid('getSelections');
//	if (rowData.id == '') {
//		tip('请选择查看项目');
//		return;
//	}
	
	if (!rowsData || rowsData.length == 0) {
		tip('请选择查看项目');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条记录再查看');
		return;
	}
    url += '&load=detail&id='+rowsData[0].id;
	createdetailwindow(title,url,width,height);
}
/**
 * 执行保存
 * 
 * @param url
 * @param gridname
 */
function saveObj() {
	$('#btn_sub', iframe.document).click();
};

/**
 * 执行操作
 * 
 * @param url
 * @param index
 */
function doSubmit(url,name) {
	gridname=name;
	$.ajax({
		async : false,
		cache : false,
		type : 'POST',
		url : url,// 请求的action路径
		error : function() {// 请求失败处理函数
		},
		success : function(data) {
			if (data.success) {
				var msg = data.msg;
				tip(msg);
				reloadTable();
			}else{
				tip(data.msg);
			}
		}
	});
};
/**
 * 提示信息
 */
function tip(msg) {
	$.dialog.setting.zIndex = 1980;
	$.messager.show({
		title : '提示信息',
		msg : msg,
		timeout : 1000 * 1.5
	});
};
/**
 * 创建询问窗口
 * 
 * @param title
 * @param content
 * @param url
 */
function createdialog(title, content, url,name) {
	$.dialog.confirm(content, function(){
		//doSubmit(url,name);
		rowid = '';
	}, function(){
	});
}
/**
 * 创建添加或编辑窗口
 * 
 * @param title
 * @param addurl
 * @param saveurl
 */
function createwindow(title, addurl,width,height) {
	width = width?width:700;
	height = height?height:400;
	if(width=="100%" || height=="100%"){
		width = document.body.offsetWidth;
		height =document.body.offsetHeight-100;
	}
	if(typeof(windowapi) == 'undefined'){
		$.dialog({
			content: 'url:'+addurl,
			lock : true,
			width:width,
			height:height,
			title:title,
			opacity : 0.3,
			cache:false,
		    ok: function(){
		    	iframe = this.iframe.contentWindow;
				saveObj();
				return false;
		    },
		    
		    cancelVal: '关闭',
		    cancel: true /*为true等价于function(){}*/
		});
	}else{
		W.$.dialog({
			content: 'url:'+addurl,
			lock : true,
			width:width,
			height:height,
			parent:windowapi,
			title:title,
			opacity : 0.3,
			cache:false,
		    ok: function(){
		    	iframe = this.iframe.contentWindow;
				saveObj();
				return false;
		    },
		    cancelVal: '关闭',
		    cancel: true /*为true等价于function(){}*/
		});
	}
};


/**
 * 创建工作流程的处理页面
 * @param title
 * @param addurl
 * @param saveurl
 */
function createtaskwindow(title,addurl,width,height) {
	width = width?width:700;
	height = height?height:400;
	if(width=="100%" || height=="100%"){
		width = document.body.offsetWidth;
		height =document.body.offsetHeight-100;
	}
	if(typeof(windowapi) == 'undefined'){
		$.dialog({
			content: 'url:'+addurl,
			lock : true,
			width:width,
			height:height,
			title:title,
			opacity : 0.3,
			cache:false,
		    button:[{
		    	name :'通过',
		    	callback : function(){
		    		iframe = this.iframe.contentWindow;
		    		var taskId = $('#taskId',iframe.document).val();
		    		// 设置流程变量
		    		taskComplete(taskId, [{
						key: 'hrPass',
						value: true,
						type: 'B'
					}],'请假流程办理','确定要通过吗?');
					return false;
		    	}
		    },{
		    	name:'打回',
		    	callback : function(){
		    		iframe = this.iframe.contentWindow;
		    		var taskId = $('#taskId',iframe.document).val();
		    		// 设置流程变量
		    		taskComplete(taskId, [{
						key: 'hrPass',
						value: false,
						type: 'B'
					}],'请假流程办理','确定要打回吗?');
		    		return false;
		    	}
		    }],
		    cancelVal: '关闭',
		    cancel: true /*为true等价于function(){}*/
		});
	}else{
		W.$.dialog({
			content: 'url:'+addurl,
			lock : true,
			width:width,
			height:height,
			parent:windowapi,
			title:title,
			opacity : 0.3,
			cache:false,
			button:[{
		    	name :'通过',
		    	callback : function(){
		    		iframe = this.iframe.contentWindow;
		    		// 设置流程变量
		    		taskComplete(taskId, [{
						key: 'hrPass',
						value: true,
						type: 'B'
					}],'请假流程办理','确定要通过吗?');
					return false;
		    	}
		    },{
		    	name:'打回',
		    	callback : function(){
		    		iframe = this.iframe.contentWindow;
		    		var taskId = $('#taskId',iframe.document).val();
		    		// 设置流程变量
		    		taskComplete(taskId, [{
						key: 'hrPass',
						value: false,
						type: 'B'
					}],'请假流程办理','确定要打回吗?');
		    		return false;
		    	}
		    }],
		    cancelVal: '关闭',
		    cancel: true /*为true等价于function(){}*/
		});
	}
};


/**
 * 完成任务
 * @param {Object} taskId
 */
function taskComplete(taskId, variables,title,msg) {
    var dialog = this;
    
	// 转换JSON为字符串
    var keys = "", values = "", types = "";
	if (variables) {
		$.each(variables, function() {
			if (keys != "") {
				keys += ",";
				values += ",";
				types += ",";
			}
			keys += this.key;
			values += this.value;
			types += this.type;
		});
	}
	
	doAjax('leaveController.do?complete&taskId='+taskId, {'keys': keys,'values': values,'types': types}, title, msg);
	
}

/**
 * 查看时的弹出窗口
 * 
 * @param title
 * @param addurl
 * @param saveurl
 */
function createdetailwindow(title, addurl,width,height) {
	width = width?width:700;
	height = height?height:400;
	if(width=="100%" || height=="100%"){
		width = document.body.offsetWidth;
		height =document.body.offsetHeight-100;
	}
	if(typeof(windowapi) == 'undefined'){
		$.dialog({
			content: 'url:'+addurl,
			lock : true,
			width:width,
			height: height,
			title:title,
			opacity : 0.3,
			cache:false, 
		    cancelVal: '关闭',
		    cancel: true /*为true等价于function(){}*/
		});
	}else{
		W.$.dialog({
			content: 'url:'+addurl,
			lock : true,
			width:width,
			height: height,
			parent:windowapi,
			title:title,
			opacity : 0.3,
			cache:false, 
		    cancelVal: '关闭',
		    cancel: true /*为true等价于function(){}*/
		});
	}
	
};

/**
 * 创建不带按钮的窗口
 * 
 * @param title
 * @param addurl
 * @param saveurl
 */
function openwindow(title, url,name, width, height) {
	gridname=name;
	if (typeof (width) == 'undefined'&&typeof (height) != 'undefined')
	{
		$.dialog({
			content: 'url:'+url,
			title : title,
			cache:false,
			lock : true,
			width: 'auto',
			opacity : 0.3,
		    height: height,
		    cancelVal: '关闭',
		    cancel: true /*为true等价于function(){}*/,
		    ok: function(){
		    	iframe = this.iframe.contentWindow;
				saveObj();
				return false;
		    }
		});
	}
	if (typeof (height) == 'undefined'&&typeof (width) != 'undefined')
	{
		$.dialog({
			content: 'url:'+url,
			title : title,
			lock : true,
			width: width,
			cache:false,
		    height: 'auto',
		    opacity : 0.3,
		    cancelVal: '关闭',
		    ok: function(){
		    	iframe = this.iframe.contentWindow;
				saveObj();
				return false;
		    },
		    cancel: true /*为true等价于function(){}*/
		});
	}
	if (typeof (width) == 'undefined'&&typeof (height) == 'undefined')
	{
	$.dialog({
		content: 'url:'+url,
		title : title,
		lock : true,
		width: 'auto',
		cache:false,
	    height: 'auto',
	    opacity : 0.3,
	    cancelVal: '关闭',
	    ok: function(){
	    	iframe = this.iframe.contentWindow;
			saveObj();
			return false;
	    },
	    cancel: true /*为true等价于function(){}*/
	});
	}
	
	if (typeof (width) != 'undefined'&&typeof (height) != 'undefined')
	{
	$.dialog({
		width: width,
	    height:height,
		content: 'url:'+url,
		title : title,
		cache:false,
		lock : true,
		cancelVal: '关闭',
		opacity : 0.3,
		ok: function(){
	    	iframe = this.iframe.contentWindow;
			saveObj();
			return false;
	    },
	    cancel: true /*为true等价于function(){}*/
	});
	}
};
/**
 * 删除
 * 
 * @param url
 * @param name
 *            列表名称(唯一)
 * @param msg
 *            操作提示
 * @param delFn
 *            自定义的回调实现
 */
function doDelete(url, name, msg, delFn) {
	if (msg == null || msg == '' || msg == 'null') {
		msg = '您确定要删除数据吗?';
	}
	$.messager.confirm("操作", msg, function(r) {
		if (r) {
			function _delFn(param) {
				request({
//					data : {
//						"id" : param
//					},
					url : url
				}, function(rs) {
					deleteCallback(rs);
				});
			}

			delFn = delFn || _delFn;
			delFn(url);
		}
	});
};

/**
 * 异步请求
 * @param url
 * @param data
 * @param name
 * @param msg
 * @param ajaxFn
 */
function doAjax(url,data, name, msg,ajaxFn) {
	if (msg == null || msg == '' || msg == 'null') {
		msg = '您确定要操作吗?';
	}
	$.messager.confirm(name || "操作", msg, function(r) {
		if (r) {
			function _ajaxFn(param) {
				request({
					data : data,
					url : url
				}, function(rs) {
					ajaxCallback(rs);
				});
			}

			ajaxFn = ajaxFn || _ajaxFn;
			ajaxFn(url);
		}
	});
};

function ajaxCallback(rs){
	deleteCallback(rs);
}
// 数据删除后的callback，默认为查询
function deleteCallback(rs) {
	if (rs.success) {
		// $.messager.alert("提示",rs.msg || "删除数据成功。");
		$.messager.show({
			title : '操作提示',
			msg : rs.msg || "删除数据成功。",
			timeout : 1200,
			showType : 'slide'
		});
		reloadTable();
	} else {
		$.messager.show({
			title : '操作提示',
			msg : rs.msg || "删除数据失败。",
			timeout : 1200,
			showType : 'slide'
		});
	}

}

/**
 * ajax 统一请求
 * 
 * @param options
 * @param callback
 */
function request(options, callback) {
	var conf = {
		type : "POST",
		url : options.url,
		dataType : "json",
		async : false,
		data : {},
		success : function(rs) {
			try {
				if (callback) {
					callback.apply(this, [ rs ]);
				}
			} finally {
				// _self.pp.util.hideMask();
			}
		},
		error : function() {
			// _self.pp.util.hideMask();
			// _self.pp.util.showMsg("请求后台","请求失败.");
		}
	}
	$.extend(conf, options);

	if (!conf.url) {
		// _self.pp.util.showMsg("参数错误","url为空，请设置。");
	}

	$.ajax(conf);
};

/**
 * 退出确认框
 * 
 * @param url
 * @param content
 * @param index
 */
function exit(url, content) {
	$.dialog.confirm(content, function(){
		window.location = url;
	}, function(){
	});
}

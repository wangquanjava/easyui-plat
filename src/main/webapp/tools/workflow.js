/**
 * 工作流程节点图片查看
 */

function graphTrace(title,url) {
//	var _defaults = {
//		        srcEle: this,
//		        pid: $(this).attr('pid')
//	    	};
//    var opts = $.extend(true, _defaults, options);
    
    $.dialog({
		content: "<img src='" + url + "' />",
		lock : true,
		width:800,
		height:350,
		title: title ||'当前处理流程查看',
		opacity : 0.8,
		cache:false,
	    cancelVal: '关闭',
	    cancel: true
	});
    
}
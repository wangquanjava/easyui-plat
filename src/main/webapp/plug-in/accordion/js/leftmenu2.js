﻿$(function () {
    var ctx = $('#ctx').val();
    $('#aa').accordion({
        fit: true,
        onSelect: function (t, index) {
            var pp = $('#aa').accordion('getSelected');
            loadMenu(pp.attr('id'),ctx);
        }
    });
    tabClose();
    tabCloseEven() ;
});

function loadMenu(pid, ctx) {
    //异步加载树
    $('#tree_'+pid).tree({
        checkbox: false,
        url: "indexController.do?findChrids&pid=" + pid,
        animate: true,
        lines: true,
        fit:true,
        onClick: function (node) {
            var state = node.state;
            var attribute=JSON.parse(node.attributes);
//            console.log(attribute.url);
           if('1' == attribute.isLeaf){
               $.messager.progress({
                   text : '页面加载中....',
                   interval : 200
               });
                if($('#main_tab').tabs("exists",node.text)){
                    $('#main_tab').tabs('select',node.text);
                    $.messager.progress('close');
                }else{
                    $('#main_tab').tabs('add', {
                        title:  node.text,
                        href: ctx + attribute.url,
                        closable: true
                    });
                }
               $.messager.progress('close');
               tabClose();
            }
        },
        onBeforeExpand: function (node, param) {
            $('#tree_'+pid).tree('options').url = "indexController.do?findChrids&pid=" + node.id;
        }
    });
}

function addmask() {
    $.messager.progress({
        text : '页面加载中....',
        interval : 100
    });
};

function tabClose() {
    /* 双击关闭TAB选项卡 */
    $(".tabs-inner").dblclick(function() {
        var subtitle = $(this).children(".tabs-closable").text();
//        alert(subtitle);
        $('#main_tab').tabs('close', subtitle);
    })
    /* 为选项卡绑定右键 */
    $(".tabs-inner").bind('contextmenu', function(e) {
        $('#mm').menu('show', {
            left : e.pageX,
            top : e.pageY
        });

        var subtitle = $(this).children(".tabs-closable").text();

        $('#mm').data("currtab", subtitle);
        return false;
    });
}

// 绑定右键菜单事件
function tabCloseEven() {
    // 刷新
    $('#mm-tabupdate').click(function() {
        var currTab = $('#main_tab').tabs('getSelected');
        var url = $(currTab.panel('options').content).attr('src');
        $('#main_tab').tabs('update', {
            tab : currTab,
            options : {
                content : createFrame(url)
            }
        })
    })
    // 关闭当前
    $('#mm-tabclose').click(function() {
        var currtab_title = $('#mm').data("currtab");
        $('#main_tab').tabs('close', currtab_title);
    })
    // 全部关闭
    $('#mm-tabcloseall').click(function() {
        $('.tabs-inner span').each(function(i, n) {
            var t = $(n).text();
            $('#main_tab').tabs('close', t);
        });
    });
    // 关闭除当前之外的TAB
    $('#mm-tabcloseother').click(function() {
        $('#mm-tabcloseright').click();
        $('#mm-tabcloseleft').click();
    });
    // 关闭当前右侧的TAB
    $('#mm-tabcloseright').click(function() {
        var nextall = $('.tabs-selected').nextAll();
        if (nextall.length == 0) {
           // alert('后边没有啦~~');
            return false;
        }
        nextall.each(function(i, n) {
            var t = $('a:eq(0) span', $(n)).text();
            $('#main_tab').tabs('close', t);
        });
        return false;
    });
    // 关闭当前左侧的TAB
    $('#mm-tabcloseleft').click(function() {
        var prevall = $('.tabs-selected').prevAll();
        if (prevall.length == 0) {
            return false;
        }
        prevall.each(function(i, n) {
            var t = $('a:eq(0) span', $(n)).text();
            $('#main_tab').tabs('close', t);
        });
        return false;
    });

    // 退出
    $("#mm-exit").click(function() {
        $('#mm').menu('hide');
    });
}
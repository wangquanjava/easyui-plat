$(function () {
    var ctx = $('#ctx').val();
    $('#userList').datagrid({
        height: 500,
        url:ctx + '/user/list.do',
        columns: [
            [
                {field: 'userName', title: '用户名称', width: 100},
                {field: 'userAccount', title: '用户账号', width: 100},
                {field: 'userDesc', title: '用户备注', width: 100}
            ]
        ]
    });
});

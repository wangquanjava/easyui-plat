$(function() {
	$('#taskList').datagrid({
		idField : 'id',
		title : '待处理流程',
		url : 'leaveController.do?taskList&field=id,processInstanceId,taskName,applyTime,taskDefinitionKey,reason,leaveType,startTime,endTime,task,realityStartTime,realityEndTime,taskId,processInstance,assignee',
		fit : true,
		loadMsg : '数据加载中,请稍候...',
		pageSize : 10,
		pagination : true,
		pageList : [ 10, 20, 50, 100 ],
		sortName : 'id',
		sortOrder : 'desc',
		rownumbers : true,
		singleSelect : true,
		fitColumns : true,
		showFooter : true,
		frozenColumns : [ [] ],
		columns : [ [ {
			field : 'id',
			title : 'id',
			hidden : true,
			sortable : true
		}, {
			field : 'processInstanceId',
			title : '流程ID',
			width : 40,
			sortable : true,
			align:'center'
		}, {
			field : 'reason',
			title : '请假事由',
			width : 80,
			sortable : true,
			align:'center'
		}, {
			field : 'leaveType',
			title : '类型',
			width : 40,
			sortable : true,
			align:'center',
			formatter : function(value, rec, index) {
				if (value == '1') {
					return '事假'
				}
				if (value == '2') {
					return '婚假'
				}
				if (value == '3') {
					return '其它'
				} else {
					return value
				}
			}
		}, {
			field : 'startTime',
			title : '开始时间',
			width : 120,
			sortable : true,
			align:'center'
		}, {
			field : 'endTime',
			title : '结束时间',
			width : 120,
			align:'center',
			sortable : true
		}, {
			field : 'realityStartTime',
			title : '实际开始时间',
			align:'center',
			width : 120,
			sortable : true
		}, {
			field : 'realityEndTime',
			title : '实际结束时间',
			align:'center',
			width : 120,
			sortable : true
		}, {
			field : 'applyTime',
			title : '申请时间',
			align:'center',
			width : 120,
			sortable : true,
			formatter : function(value, rec, index) {
				return value;
			}
		}, {
			field : 'taskName',
			title : '当前节点',
			align:'center',
			width : 100,
			sortable : true,
			formatter : function(value, rec, index) {
				return "<a href='#' class='trace' title='点击查看流程图' onclick=graphTrace('当前节点','leaveController.do?workflow&executionId="+rec.processInstanceId +"')>" + value + '</a>';
			}
		},{
			field : 'opt',
			title : '操作',
			align:'center',
			width : 140,
			formatter : function(value, rec, index) {
				if (!rec.id) {
					return '';
				}
				var href = '';
				if(rec.assignee == null ||　rec.assignee == ''){
					href += "【<a href='#' onclick=doDelete('leaveController.do?claim&taskId=" + rec.taskId + "','taskList','你确定要签收此任务吗?')>";
					href += "签收</a>】";
				}else if(rec.taskDefinitionKey == 'user_modify'){
					href += "【<a href='#' onclick=modifytask('申请修改','leaveController.do?toTask&root=leave&taskId=" + rec.taskId + "&id=" + rec.id + "','taskList',500,250)>";
					href += "申请修改</a>】";
					href += "【<a href='#' onclick=cancelOA(" + rec.taskId + ")>";
					href += "取消</a>】";
				}else{
					href += "【<a href='#' onclick=updatetask('请假申请办理','leaveController.do?toTask&root=leave&taskId=" + rec.taskId + "','taskList',500,250)>";
					href += "办理</a>】";
				}
				return href;
			}
		} ] ],
		onLoadSuccess : function(data) {
			$("#taskList").datagrid("clearSelections");
			//节点跟踪
		    //$('.trace').click(graphTrace);
		},
		onClickRow : function(rowIndex, rowData) {
			rowid = rowData.id;
			gridname = 'taskList';
		}
	});
	$('#taskList').datagrid('getPager').pagination({
		beforePageText : '',
		afterPageText : '/{pages}',
		displayMsg : '{from}-{to}共{total}条',
		showPageList : true,
		showRefresh : true
	});
	$('#taskList').datagrid('getPager').pagination({
		onBeforeRefresh : function(pageNumber, pageSize) {
			$(this).pagination('loading');
			$(this).pagination('loaded');
		}
	});
	
    
});
function reloadTable() {
	try {
		$('#' + gridname).datagrid('reload');
		$('#' + gridname).treegrid('reload');
	} catch (ex) {
	}
}
function reloadtaskList() {
	$('#taskList').datagrid('reload');
}
function gettaskListSelected(field) {
	return getSelected(field);
}
function getSelected(field) {
	var row = $('#' + gridname).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}
function gettaskListSelections(field) {
	var ids = [];
	var rows = $('#taskList').datagrid('getSelections');
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};
function taskListsearch() {
	var queryParams = $('#taskList').datagrid('options').queryParams;
	$('#taskListtb').find('*').each(function() {
		queryParams[$(this).attr('name')] = $(this).val();
	});
	$('#taskList').datagrid({
		url : 'leaveController.do?taskList&field=id,processInstanceId,reason,leaveType,startTime,endTime,realityStartTime,realityEndTime,task,',
		pageNumber : 1
	});
}
function dosearch(params) {
	var jsonparams = $.parseJSON(params);
	$('#taskList').datagrid({
		url : 'leaveController.do?taskList&field=id,processInstanceId,reason,leaveType,startTime,endTime,realityStartTime,realityEndTime,task,',
		queryParams : jsonparams
	});
}
function taskListsearchbox(value, name) {
	var queryParams = $('#taskList').datagrid('options').queryParams;
	queryParams[name] = value;
	queryParams.searchfield = name;
	$('#taskList').datagrid('reload');
}
$('#taskListsearchbox').searchbox({
	searcher : function(value, name) {
		taskListsearchbox(value, name);
	},
	menu : '#taskListmm',
	prompt : '请输入查询关键字'
});
function searchReset(name) {
	$("#" + name + "tb").find(":input").val("");
	taskListsearch();
}


function modifytask(title,url,id,width,height) {
//	var rowsData = $('#'+id).datagrid('getSelections');
//	if (!rowsData || rowsData.length==0) {
//		tip('请选择要办理的流程!');
//		return;
//	}
//	if (rowsData.length>1) {
//		tip('请选择一条记录再办理!');
//		return;
//	}
//	url += '&id='+rowsData[0].id;
	createmodifytaskwindow(title,url,width,height);
}

function cancelOA(taskId){
	taskComplete(taskId,[{
		key: 'reApply',
		value: false,
		type: 'B'
	}],'取消申请','你确定要取消此请假流程吗?');
}

/**
 * 创建工作流程的处理页面
 * @param title
 * @param addurl
 * @param saveurl
 */
function createmodifytaskwindow(title,addurl,width,height) {
	width = width?width:700;
	height = height?height:400;
	if(width=="100%" || height=="100%"){
		width = document.body.offsetWidth;
		height =document.body.offsetHeight-100;
	}
	$.dialog({
		content: 'url:'+addurl,
		lock : true,
		width:width,
		height:height,
		title:title,
		opacity : 0.3,
		cache:false,
	    button:[{
	    	name :'提交',
	    	callback : function(){
	    		iframe = this.iframe.contentWindow.document;
	    		var taskId = $('#taskId',iframe).val();
	    		// 设置流程变量
	    		taskComplete(taskId, [{
					key: 'reApply',
					value: true,
					type: 'B'
				},{
					key: 'leaveType',
					value: $('#leaveType',iframe).val(),
					type: 'S'
				},{
					key: 'reason',
					value: $('#reason',iframe).val(),
					type: 'S'
				}],'请假流程修改','确定要修改后提交吗?');
				return false;
	    	}
	    }],
	    cancelVal: '关闭',
	    cancel: true /*为true等价于function(){}*/
	});
};

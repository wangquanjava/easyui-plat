<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="rolesList" title="角色维护" actionUrl="${pageContext.request.contextPath }/rolesController.do?list" idField="id" fit="true" sortName="id" sortOrder="desc" queryMode="group">
   <t:dgCol title="id" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="角色编码" field="roleId" width="100"></t:dgCol>
   <t:dgCol title="角色名称" field="roleName" width="100"></t:dgCol>
   <t:dgCol title="角色描述" field="roleDesc" width="100"></t:dgCol>
   <t:dgCol title="是否启用" field="enabled" query="true" width="100" dictionary="is_user" queryMode="single"></t:dgCol>
   <t:dgCol title="是否是管理员" field="issys" width="100" dictionary="is_user_e"></t:dgCol>
   <t:dgCol title="模块名称" field="module" width="100" hidden="false"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="${pageContext.request.contextPath }/rolesController.do?doDelete&id={id}" />
   <t:dgOpenOpt title="修改" url="${pageContext.request.contextPath }/rolesController.do?toEdit&root=roles&id={id}" width="700" height="380"></t:dgOpenOpt>
   <t:dgToolBar title="添加" icon="icon-add" url="${pageContext.request.contextPath }/rolesController.do?toEdit&root=roles" funname="add" width="700" height="400"></t:dgToolBar>
   <t:dgToolBar title="修改" icon="icon-edit" url="${pageContext.request.contextPath }/rolesController.do?toEdit&root=roles" funname="update" width="700" height="400"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="${pageContext.request.contextPath }/rolesController.do?toEdit&root=roles" funname="detail" width="700" height="400"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
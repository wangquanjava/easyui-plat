<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>权限维护</title>
  <t:base type="jq,easyui,tools,DatePicker"></t:base>
 </head>
 <body style="overflow-y: auto">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="${pageContext.request.contextPath }/rolesController.do?doSave">
			<input id="id" name="id" type="hidden" value="${entity.id }">
			<table cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right" width="25%">
						<label class="Validform_label">
							角色代码:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="roleId" name="roleId" 
							   value="${entity.roleId}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							角色名称:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="roleName" name="roleName" ignore="ignore"
							   value="${entity.roleName}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							角色描述:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="roleDesc" name="roleDesc" ignore="ignore"
							   value="${entity.roleDesc}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							是否启用:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="enabled" typeGroupCode="is_user_e" hasLabel="false" defaultVal="${entity.enabled == true?'1':'0'}"/>
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							是否是管理员:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="issys" typeGroupCode="is_user_e" hasLabel="false" defaultVal="${entity.issys == true?'1':'0'}"/>
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
			     <td align="right">
			      <label class="Validform_label">
			       	角色权限:
			      </label>
			     </td>
			     <td class="value" nowrap>
			      <input name="authorityIds" type="hidden" value="" id="authorityIds" >
			      <textarea rows="10" cols="10" class="inputxt" name="authorityName" id="authorityName" readonly="readonly" datatype="*">
      					
      			  </textarea>
			      <t:choose hiddenName="authorityIds" hiddenid="authorityId" url="${pageContext.request.contextPath}/rolesController.do?toListPage&pathroot=authors&page=authorityList" name="authorsList" icon="icon-choose" title="角色列表" textname="authorityName" isclear="true" width="600" height="380"></t:choose>
			      <span class="Validform_checktip">权限可多选</span>
			     </td>
			    </tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							模块名称:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="module" name="module" ignore="ignore"
							   value="${entity.module}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
 </body>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/easyui-tags"%>
<html>
<head>
  <title>角色列表</title>
  <t:base type="jq,easyui,tools"></t:base>
 </head>
 <body style="overflow-y: auto" >
 	<t:datagrid name="roleList" title="角色维护" actionUrl="${pageContext.request.contextPath }/rolesController.do?list" idField="id" fit="true" sortName="id" sortOrder="desc" checkbox="true" showRefresh="false">
	   <t:dgCol title="id" field="id" hidden="false"></t:dgCol>
	   <t:dgCol title="角色编码" field="roleId" width="100"></t:dgCol>
	   <t:dgCol title="角色名称" field="roleName" width="100"></t:dgCol>
  </t:datagrid>
 </body>
</html>

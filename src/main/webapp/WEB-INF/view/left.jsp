<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%> 
<link rel="stylesheet" href="${pageContext.request.contextPath}/plug-in/accordion/css/icons.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/plug-in/accordion/css/accordion.css" type="text/css"></link>
<script type="text/javascript" src="${pageContext.request.contextPath}/plug-in/accordion/js/leftmenu2.js"></script>
<input type="hidden" value="${pageContext.request.contextPath}" id="ctx"/>
<div id="aa" class="" style="" data-options="fit:true,border:false">
    <c:forEach items="${m}" var="v">
        <div title="${v.resourceName}" id="${v.resourceId}" data-options="iconCls:'system'" style="overflow:auto;padding:2px;height: 1000px">
             <div class="panel-body accordion-body" data-options="fit:true,border:true" style="background:#fafafa">
                 <ul id="tree_${v.resourceId}" style="" class="easyui-tree"></ul>
             </div>
        </div>
    </c:forEach>
</div>

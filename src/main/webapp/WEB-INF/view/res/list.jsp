<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<div class="easyui-layout" fit="true" id="system_function_functionList">
  <div region="center" style="padding:1px;">
<t:datagrid name="resList" title="菜单维护" actionUrl="resController.do?functionGrid" idField="id" sortName="id" sortOrder="desc" treegrid="true" pagination="false">
   <t:dgCol title="id" field="id" treefield="id" hidden="false"></t:dgCol>
   <t:dgCol title="菜单名称" field="resourceName" width="100" treefield="text"></t:dgCol>
   <t:dgCol title="链接" field="resourceString" width="100" treefield="src"></t:dgCol>
   <t:dgCol title="父级编号" field="pid"  treefield="parentId" width="80"></t:dgCol>
   <t:dgCol title="菜单顺序" field="rsort"  treefield="order" width="80"></t:dgCol>
   <t:dgCol title="图标" field="icon"  treefield="code" image="true" width="80"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="resController.do?doDelete&id={id}" />
   <t:dgOpenOpt title="修改" url="${pageContext.request.contextPath}/resController.do?toEdit&root=res&id={id}" width="700" height="350"></t:dgOpenOpt>
   <t:dgToolBar title="添加" icon="icon-add" url="${pageContext.request.contextPath}/resController.do?toEdit&root=res" funname="add" height="350" width="700"></t:dgToolBar>
   <t:dgToolBar title="修改" icon="icon-edit" url="${pageContext.request.contextPath}/resController.do?toEdit&root=res" funname="update" height="350" width="700"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="${pageContext.request.contextPath}/resController.do?toEdit&root=res" funname="detail" height="350" width="700"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
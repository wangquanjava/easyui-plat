<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>菜单维护</title>
  <t:base type="jq,easyui,tools"></t:base>
  <script type="text/javascript">
  	$(function(){
  		$('#cc').combotree({
			url : 'resController.do?getParent',
			panelHeight:'auto',
			onClick: function(node){
				$("#pid").val(node.id);
			}
		});
  		
  		if($('#level').val()=='2'){
			$('#pfun').show();
		}else{
			$('#pfun').hide();
		}
		
  		$('#level').change(function(){
			if($(this).val()=='2'){
				$('#pfun').show();
				var t = $('#cc').combotree('tree');
				var nodes = t.tree('getRoots');
				if(nodes.length>0){
					$('#cc').combotree('setValue', nodes[0].id);
					$("#resourceId").val(nodes[0].id);
				}
			}else{
				var t = $('#cc').combotree('tree');
				var node = t.tree('getSelected');
				if(node){
					$('#cc').combotree('setValue', null);
				}
				$('#pfun').hide();
			}
		});
  	})
  </script>
 </head>
 <body style="overflow-y: auto;">
  <t:formvalid formid="formobj" dialog="true" layout="table" action="${pageContext.request.contextPath}/resController.do?doSave" >
			<input id="id" name="id" type="hidden" value="${entity.id }">
			<input id="resourceId" name="resourceId" type="hidden" value="${entity.resourceId }">
			<table cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right" width="32%">
						<label class="Validform_label">
							菜单名称:
						</label>
					</td>
					<td class="value" width="68%">
						<input class="inputxt" id="resourceName" name="resourceName" 
							   value="${entity.resourceName}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							菜单描述:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="resourceDesc" name="resourceDesc" 
							   value="${entity.resourceDesc}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							类型:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="resourceType" name="resourceType" 
							   value="${entity.resourceType}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							菜单链接:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="resourceString" name="resourceString" 
							   value="${entity.resourceString}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							是否启用:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="enabled" name="enabled" 
							   value="${entity.enabled}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							菜单级别:
						</label>
					</td>
					<td class="value">
						<select name="level" id="level" datatype="*">
						<option value="1"
							<c:if test="${entity.level eq 1}">selected="selected"</c:if>>
							一级菜单
						</option>
						<option value="2"
							<c:if test="${entity.level>1}"> selected="selected"</c:if>>
							下级菜单
						</option>
					</select>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							父菜单:
						</label>
					</td>
					<td class="value">
					<span id="pfun">
						<input id="cc" class="inputxt" style="width: 156px;height: 24px"
						<c:if test="${entity.parentResources.level eq 1}">
							value="${entity.pid}"</c:if>
						<c:if test="${entity.parentResources.level > 1}">
							value="${entity.parentResources.resourceName}"</c:if>	>
						<input id ="pid" name="pid" type="hidden"
							value="${entity.pid}">
					</span>
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							排序:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="rsort" name="rsort" 
							   value="${entity.rsort}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							是否是叶子:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="isLeaf" name="isLeaf" 
							   value="${entity.isLeaf}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
 </body>
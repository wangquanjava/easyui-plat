<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>认证维护</title>
  <t:base type="jq,easyui,tools,DatePicker"></t:base>
 </head>
 <body style="overflow-y: hidden">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="${pageContext.request.contextPath }/authorsController.do?doSave">
			<input id="id" name="id" type="hidden" value="${entity.id }">
			<table cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right" width="25%">
						<label class="Validform_label">
							认证id:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="authorityId" name="authorityId" 
							   value="${entity.authorityId}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" width="25%">
						<label class="Validform_label">
							认证名称:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="authorityName" name="authorityName" ignore="ignore"
							   value="${entity.authorityName}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" width="25%">
						<label class="Validform_label">
							认证描述:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="authorityDesc" name="authorityDesc" ignore="ignore"
							   value="${entity.authorityDesc}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" width="25%">
						<label class="Validform_label">
							是否启用:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="enabled" name="enabled" ignore="ignore"
							   value="${entity.enabled}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" width="25%">
						<label class="Validform_label">
							是否管理员:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="issys" name="issys" ignore="ignore"
							   value="${entity.issys}" datatype="n">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right" width="25%">
						<label class="Validform_label">
							模块:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="module" name="module" ignore="ignore"
							   value="${entity.module}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
 </body>
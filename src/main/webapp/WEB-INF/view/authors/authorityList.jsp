<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/easyui-tags"%>
<html>
<head>
<title>权限列表</title>
  <t:base type="jq,easyui,tools"></t:base>
</head>
<body style="overflow-y: auto" >
<t:datagrid name="authorsList" title="认证维护" actionUrl="${pageContext.request.contextPath }/authorsController.do?list" idField="id" fit="true" sortName="id" sortOrder="desc" checkbox="true" showRefresh="false">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="权限代码" field="authorityId" width="120"></t:dgCol>
   <t:dgCol title="权限名称" field="authorityName" width="120"></t:dgCol>
  </t:datagrid>
</body>
</html>

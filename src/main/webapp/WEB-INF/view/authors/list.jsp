<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/easyui-tags"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="authorsList" title="认证维护" actionUrl="${pageContext.request.contextPath }/authorsController.do?list" idField="id" fit="true" sortName="id" sortOrder="desc">
   <t:dgCol title="编号" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="权限代码" field="authorityId" width="80"></t:dgCol>
   <t:dgCol title="权限名称" field="authorityName" width="80"></t:dgCol>
   <t:dgCol title="描述" field="authorityDesc" width="80"></t:dgCol>
   <t:dgCol title="是否启用" field="enabled" width="30"></t:dgCol>
   <t:dgCol title="是否管理员" field="issys" width="30"></t:dgCol>
   <t:dgCol title="模块" field="module" width="100" hidden="false"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="${pageContext.request.contextPath }/authorsController.do?doDelete&id={id}" />
   <t:dgOpenOpt title="修改" url="${pageContext.request.contextPath }/authorsController.do?toEdit&root=authors&id={id}" width="700" height="380"></t:dgOpenOpt>
   <t:dgFunOpt funname="setfunbyrole(authorityId,authorityName)" title="权限设置"></t:dgFunOpt>
   <t:dgToolBar title="添加" icon="icon-add" url="${pageContext.request.contextPath }/authorsController.do?toEdit&root=authors" funname="add" width="700" height="380"></t:dgToolBar>
   <t:dgToolBar title="修改" icon="icon-edit" url="${pageContext.request.contextPath }/authorsController.do?toEdit&root=authors" funname="update" width="700" height="380"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="${pageContext.request.contextPath }/authorsController.do?toEdit&root=authors" funname="detail" width="700" height="380"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <input type="hidden" value="${pageContext.request.contextPath}" id="ctx"/>
 <div data-options="region:'east',iconCls:'folder',split:true" title='权限设置' style="width:280px;">
 <div tools="#tt" class="easyui-panel" style="padding: 0px;" fit="true" border="false" id="function-panel">
  	
  </div>
 </div>
 <script type="text/javascript">
 	function setfunbyrole(authorityId,authorityName){
 		$("#function-panel").panel(
 				{
 					//title :authorityName+":当前权限",
 					href:$('#ctx').val()+"/authorsController.do?rolesFun&authorityId=" + authorityId
 				}
 			);
 		$('#function-panel').panel("refresh" );
 	}
 </script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<form class="login_form" id="login_form" name="login_form" action="${pageContext.request.contextPath}/j_spring_security_check" method="post">
		    <!-- 显示登录失败信息 -->
			<div class="error ${param.error == true ? '' : 'hide'}"> 
           		登陆失败<br> 
    			${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message} 
    		</div> 

    	    
            <table width="100%" border="0" cellspacing="0" cellpadding="0" summary="Sign in form">
                <tr>
                    <th><label for="username" class="field">用户名称</label></th>
                    <td><input id="username" maxlength="64" style="width: 170px" name="j_username" 
					     class="text" type="text" value="${sessionScope['SPRING_SECURITY_LAST_USERNAME']}" />
					</td>
                </tr>
                <tr>
                    <th><label for="password" class="field">用户密码</label></th>
                    <td><input id="password" maxlength="64" style="width: 170px" name="j_password" class="text" type="password" /></td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td><input id="remember" name="_spring_security_remember_me" value="true" type="checkbox" class="field" />
                    <label for="remember" class="field">两周之内记住我！</label></td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td align="center"><input name="login" value="登 录" type="submit" class="buttons" /></td>
                </tr>
            </table>           
 

        </form>
</body>
</html>
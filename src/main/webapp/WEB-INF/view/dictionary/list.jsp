<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>

<script type="text/javascript">
function typeGridTree_AddType() {
	var treeCtrlId = "typeGridTree";
	var node = $("#"+treeCtrlId).treegrid('getSelected');
	if (node == null) {
		tip("请选择一个数据字典组");
		return;
	}
	if (node.state == "closed" || node.children) {
	} else {//叶结点
		node = $("#"+treeCtrlId).treegrid('getParent', node.id); //获取当前节点的父节点
	}
	var groupid = node.id.substring(1);
	add("参数值录入("+node.text+")", "dictionaryController.do?toTypeEdit&typegroupid="+groupid, treeCtrlId,700,200);
}
function typeGridTree_UpdateType(type) {
	var treeCtrlId = "typeGridTree";
	var node = $("#"+treeCtrlId).treegrid('getSelected');
	if (node == null) {
		tip("请选择一个编辑对象。");
		return;
	}
	var nodeid = node.id.substring(1);
	if (node.state == "closed" || node.children) {
		if('detail'== type){
			createdetailwindow("字典编辑", "dictionaryController.do?toEditGroup&root=dictionary&id="+nodeid);
		}else{
			createwindow("字典编辑", "dictionaryController.do?toEditGroup&root=dictionary&id="+nodeid);
		}
	} else {//叶结点
		var pnode = $("#"+treeCtrlId).treegrid('getParent', node.id); //获取当前节点的父节点
		var groupid = pnode.id.substring(1);
		if('detail'== type){
			createdetailwindow("参数值编辑", "dictionaryController.do?toTypeEdit&typegroupid="+groupid+"&id="+nodeid);
		}else{
			createwindow("参数值编辑", "dictionaryController.do?toTypeEdit&typegroupid="+groupid+"&id="+nodeid);
		}
	}
}
</script>
<div class="easyui-layout" fit="true" id="system_function_functionList">
  <div region="center" style="padding:1px;">
<t:datagrid name="typeGridTree" title="字典维护" actionUrl="dictionaryController.do?typeGridTree" idField="id" sortName="id" sortOrder="desc" treegrid="true" pagination="false">
   <t:dgCol title="编号" field="id" treefield="id" hidden="false"></t:dgCol>
   <t:dgCol title="字典名称" field="typename" width="100" treefield="text"></t:dgCol>
   <t:dgCol title="字典编码" field="code" width="100" treefield="code"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="dictionaryController.do?doDeleteGroup&id={id}" />
   <t:dgToolBar title="字典项添加" icon="icon-add" url="dictionaryController.do?toEditGroup&root=dictionary" funname="add" height="350" width="700"></t:dgToolBar>
   <t:dgToolBar title="参数值添加" icon="icon-add" funname="typeGridTree_AddType"></t:dgToolBar>
   <t:dgToolBar title="修改" icon="icon-edit" funname="typeGridTree_UpdateType" height="350" width="700"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" funname="typeGridTree_UpdateType(detail)" height="350" width="700"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
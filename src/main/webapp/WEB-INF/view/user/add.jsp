<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>用户信息</title>
	<t:base type="jq,easyui,tools"></t:base>
</head>
<body style="overflow-y: auto" scroll="no">
	<t:formvalid formid="formobj" dialog="true" layout="table" usePlugin="password" action="${pageContext.request.contextPath}/userController.do?doSaveUser">
			<input id="id" name="id" type="hidden" value="${entity.id }">
			
			<table cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right" nowrap="nowrap" width="25%">
						<label class="Validform_label">
							用户ID:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="userId" name="userId"
							   value="${entity.userId}" datatype="*2-10" nullmsg="请输入用户ID">
						<span class="Validform_checktip">用户ID范围在2~10位字符</span>
					</td>
				</tr>
				<tr>
					<td align="right" nowrap="nowrap" width="25%">
						<label class="Validform_label">
							用户名称:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="userName" name="userName"
							   value="${entity.userName}" datatype="*2-10" nullmsg="请输入用户姓名">
						<span class="Validform_checktip">用户名称范围在2~10位字符</span>
					</td>
				</tr>
				<tr>
					<td align="right" nowrap="nowrap" width="25%">
						<label class="Validform_label">
							用户账号:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="userAccount" name="userAccount"
							   value="${entity.userAccount}" datatype="*" nullmsg="请输入用户账号" errormsg="邮箱格式不正确">
						<span class="Validform_checktip">用户账号格式不对(只能为常用邮箱)</span>
					</td>
				</tr>
				<tr>
					<td align="right" nowrap="nowrap" width="25%">
						<label class="Validform_label">
							密码:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="userPassword" name="userPassword" type="password" maxlength="30"
							   value="${entity.userPassword}" datatype="*1-32" nullmsg="请输入密码">
						<span class="Validform_checktip">密码6~32位字符</span>
					</td>
				</tr>
				<tr>
					<td align="right" nowrap="nowrap" width="25%">
						<label class="Validform_label">
							确认密码:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="userpassword2" name="userpassword2" type="password" maxlength="30"
							   value="${entity.userPassword}" datatype="*1-32" recheck="userPassword" nullmsg="请输入确认密码">
						<span class="Validform_checktip">确认密码6~32位字符</span>
					</td>
				</tr>
							 <tr>
			     <td align="right">
			      <label class="Validform_label">
			       	角色:
			      </label>
			     </td>
			     <td class="value" nowrap>
			      <input name="roleIds" type="hidden" value="" id="roleId" >
			      <textarea rows="10" cols="10" class="inputxt" name="roleName" id="roleName" readonly="readonly" datatype="*">
      					${entity.roleName }
      			  </textarea>
			      <t:choose hiddenName="roleId" hiddenid="roleId" url="userController.do?toListPage&pathroot=roles&page=rolesList" name="roleList" icon="icon-choose" title="角色列表" textname="roleName" isclear="true" width="600" height="380"></t:choose>
			      <span class="Validform_checktip">角色可多选</span>
			     </td>
			    </tr>
				<tr>
					<td align="right" nowrap="nowrap" width="25%">
						<label class="Validform_label">
							备注:
						</label>
					</td>
					<td class="value" width="75%">
						<input class="inputxt" id="userDesc" name="userDesc" maxlength="30"
							   value="${entity.userDesc}">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
</body>
</html>
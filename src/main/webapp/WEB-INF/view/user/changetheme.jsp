<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>皮肤选择</title>
	<t:base type="jq,easyui,tools"></t:base>
</head>
<body style="overflow-y: auto" scroll="no">
	<select id="changetheme" onchange="changeThemeFun(this.value)">
		<option value="default">默认</option>
		<option value="green">绿</option>
		<option value="gray">灰</option>
		<option value="pink">红</option>
		<option value="orange">橘黄</option>
	</select>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/easyui-tags"%>
<div class="easyui-layout" fit="true">
<div region="center" style="padding:1px;">
<t:datagrid actionUrl="${pageContext.request.contextPath}/userController.do?list" name="userList" title="用户维护" idField="id" sortName="id" sortOrder="desc" queryMode="group">
	<t:dgCol field="id" title="用户id" hidden="false"></t:dgCol>
	<t:dgCol field="userId" title="用户ID" width="100"></t:dgCol>
	<t:dgCol field="userName" title="用户名称" width="100" queryMode="single" query="true"></t:dgCol>
	<t:dgCol field="userAccount" title="用户账号" width="200" queryMode="single" query="true"></t:dgCol>
	<t:dgCol field="userDesc" title="用户备注" width="300"></t:dgCol>
	<t:dgCol title="操作" field="opt" width="100"></t:dgCol>
	<t:dgDelOpt title="删除" url="${pageContext.request.contextPath}/userController.do?doDelete&id={id}"/>
	<t:dgOpenOpt title="修改" url="${pageContext.request.contextPath}/userController.do?toEdit&root=user&id={id}" width="750" height="400"></t:dgOpenOpt>	
	<t:dgToolBar title="添加" icon="icon-add" url="${pageContext.request.contextPath}/userController.do?toEdit&root=user" funname="add" width="750" height="400"></t:dgToolBar>
    <t:dgToolBar title="修改" icon="icon-edit" url="${pageContext.request.contextPath}/userController.do?toEdit&root=user" funname="update" width="750" height="400"></t:dgToolBar>
	<t:dgToolBar title="查看" icon="icon-search" url="${pageContext.request.contextPath}/userController.do?toEdit&root=user" funname="detail" width="750" height="400"></t:dgToolBar>
</t:datagrid>
</div>
</div>
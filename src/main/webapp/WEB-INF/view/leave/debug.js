$(function() {
	$('#runList')
			.datagrid(
					{
						idField : 'id',
						title : '已启动流程',
						url : 'leaveController.do?runList&field=id,processInstanceId,reason,leaveType,startTime,endTime,realityStartTime,realityEndTime,applyTime,taskName,assignee,',
						fit : true,
						loadMsg : '数据加载中,请稍候...',
						pageSize : 10,
						pagination : true,
						pageList : [ 10, 20, 50, 100 ],
						sortName : 'id',
						sortOrder : 'desc',
						rownumbers : true,
						singleSelect : true,
						fitColumns : true,
						showFooter : true,
						frozenColumns : [ [] ],
						columns : [ [
								{
									field : 'id',
									title : 'id',
									hidden : true,
									sortable : true
								},
								{
									field : 'processInstanceId',
									title : '流程ID',
									width : 40,
									align : 'center',
									sortable : true
								},
								{
									field : 'reason',
									title : '请假事由',
									width : 80,
									align : 'center',
									sortable : true
								},
								{
									field : 'leaveType',
									title : '类型',
									width : 40,
									align : 'center',
									sortable : true,
									formatter : function(value, rec, index) {
										if (value == '1') {
											return '事假'
										}
										if (value == '2') {
											return '婚假'
										}
										if (value == '3') {
											return '其它'
										} else {
											return value
										}
									}
								},
								{
									field : 'startTime',
									title : '开始时间',
									width : 120,
									align : 'center',
									sortable : true
								},
								{
									field : 'endTime',
									title : '结束时间',
									width : 120,
									align : 'center',
									sortable : true
								},
								{
									field : 'realityStartTime',
									title : '实际开始时间',
									width : 120,
									align : 'center',
									sortable : true
								},
								{
									field : 'realityEndTime',
									title : '实际结束时间',
									width : 120,
									align : 'center',
									sortable : true
								},
								{
									field : 'applyTime',
									title : '申请时间',
									width : 120,
									align : 'center',
									sortable : true
								},
								{
									field : 'taskName',
									title : '当前节点',
									width : 100,
									align : 'center',
									sortable : true,
									formatter : function(value, rec, index) {
										var href = "<a style='color:blue' href='#' onclick=graphTrace('当前节点','leaveController.do?workflow&executionId="
												+ rec.processInstanceId + "')>";
										return href + value + '</a>';
									}
								}, {
									field : 'assignee',
									title : '当前处理人',
									width : 100,
									align : 'center',
									sortable : true
								}, {
									field : 'opt',
									title : '操作',
									width : 100,
									align : 'center',
									formatter : function(value, rec, index) {
										if (!rec.id) {
											return '';
										}
										var href = '';
										href += process(href, value, rec, index);
										return href;
									}
								} ] ],
						onLoadSuccess : function(data) {
							$("#runList").datagrid("clearSelections");
						},
						onClickRow : function(rowIndex, rowData) {
							rowid = rowData.id;
							gridname = 'runList';
						}
					});
	$('#runList').datagrid('getPager').pagination({
		beforePageText : '',
		afterPageText : '/{pages}',
		displayMsg : '{from}-{to}共{total}条',
		showPageList : true,
		showRefresh : true
	});
	$('#runList').datagrid('getPager').pagination({
		onBeforeRefresh : function(pageNumber, pageSize) {
			$(this).pagination('loading');
			$(this).pagination('loaded');
		}
	});
});
function reloadTable() {
	try {
		$('#' + gridname).datagrid('reload');
		$('#' + gridname).treegrid('reload');
	} catch (ex) {
	}
}
function reloadrunList() {
	$('#runList').datagrid('reload');
}
function getrunListSelected(field) {
	return getSelected(field);
}
function getSelected(field) {
	var row = $('#' + gridname).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}
function getrunListSelections(field) {
	var ids = [];
	var rows = $('#runList').datagrid('getSelections');
	for (var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};
function runListsearch() {
	var queryParams = $('#runList').datagrid('options').queryParams;
	$('#runListtb').find('*').each(function() {
		queryParams[$(this).attr('name')] = $(this).val();
	});
	$('#runList')
			.datagrid(
					{
						url : 'leaveController.do?runList&field=id,processInstanceId,reason,leaveType,startTime,endTime,realityStartTime,realityEndTime,applyTime,taskName,assignee,',
						pageNumber : 1
					});
}
function dosearch(params) {
	var jsonparams = $.parseJSON(params);
	$('#runList')
			.datagrid(
					{
						url : 'leaveController.do?runList&field=id,processInstanceId,reason,leaveType,startTime,endTime,realityStartTime,realityEndTime,applyTime,taskName,assignee,',
						queryParams : jsonparams
					});
}
function runListsearchbox(value, name) {
	var queryParams = $('#runList').datagrid('options').queryParams;
	queryParams[name] = value;
	queryParams.searchfield = name;
	$('#runList').datagrid('reload');
}
$('#runListsearchbox').searchbox({
	searcher : function(value, name) {
		runListsearchbox(value, name);
	},
	menu : '#runListmm',
	prompt : '请输入查询关键字'
});
function searchReset(name) {
	$("#" + name + "tb").find(":input").val("");
	runListsearch();
}
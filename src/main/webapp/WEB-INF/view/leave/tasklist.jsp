<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>

<div class="easyui-layout" fit="true" id="system_function_functionList">
  <div region="center" style="padding:1px;">
<t:datagrid name="taskList" title="已启动流程" actionUrl="leaveController.do?taskList" idField="id" sortName="id" sortOrder="desc">
   <t:dgCol title="id" field="id" hidden="false"></t:dgCol>
   <t:dgCol title="taskDefinitionKey" field="taskDefinitionKey" hidden="false"></t:dgCol>
   <t:dgCol title="taskId" field="taskId" hidden="false"></t:dgCol>
   <t:dgCol title="流程ID" field="processInstanceId" width="40" align="center"></t:dgCol>
   <t:dgCol title="请假事由" field="reason" width="80" align="center"></t:dgCol>
   <t:dgCol title="类型" field="leaveType" width="40" dictionary="oa_type" align="center"></t:dgCol>
   <t:dgCol title="开始时间" field="startTime" width="120" align="center"></t:dgCol>
   <t:dgCol title="结束时间" field="endTime"  width="120" align="center"></t:dgCol>
   <t:dgCol title="实际开始时间" field="realityStartTime" width="120" align="center"></t:dgCol>
   <t:dgCol title="实际结束时间" field="realityEndTime" width="120" align="center"></t:dgCol>
   <t:dgCol title="申请时间" field="applyTime" width="120" align="center"></t:dgCol>
   <t:dgCol title="当前节点" field="taskName" width="100" align="center" funname="graphTrace" url="leaveController.do?workflow&executionId={processInstanceId}"></t:dgCol>
   <t:dgCol title="当前处理人" field="assignee" width="100" align="center" hidden="false"></t:dgCol>
   <t:dgCol field="opt" title="操作" width="100" align="center"></t:dgCol>
   <t:dgExtenedFormatOpt funname="processExtend" title='自定义'></t:dgExtenedFormatOpt>
   <t:dgToolBar title="查看" icon="icon-search" url="leaveController.do?toEdit&root=leave" funname="detail" height="350" width="700"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 
<script type="text/javascript">
	function processExtend(href, value, rec, index){
		if(rec.assignee == null ||　rec.assignee == ''){
			href += "【<a href='#' onclick=doDelete('leaveController.do?claim&taskId=" + rec.taskId + "','taskList','你确定要签收此任务吗?')>";
			href += "签收</a>】";
		}else if(rec.taskDefinitionKey == 'user_modify'){
			href += "【<a href='#' onclick=modifytask('申请修改','leaveController.do?toTask&root=leave&taskId=" + rec.taskId + "&id=" + rec.id + "','taskList',500,250)>";
			href += "申请修改</a>】";
			href += "【<a href='#' onclick=cancelOA(" + rec.taskId + ",'"+rec.leaveType+"','"+rec.reason+"')>";
			href += "取消</a>】";
		}else{
			href += "【<a href='#' onclick=updatetask('请假申请办理','leaveController.do?toTask&root=leave&taskId=" + rec.taskId + "&id=" + rec.id + "','taskList',500,250)>";
			href += "办理</a>】";
		}
		return href;
	}

	function modifytask(title,url,id,width,height) {
//		var rowsData = $('#'+id).datagrid('getSelections');
//		if (!rowsData || rowsData.length==0) {
//			tip('请选择要办理的流程!');
//			return;
//		}
//		if (rowsData.length>1) {
//			tip('请选择一条记录再办理!');
//			return;
//		}
//		url += '&id='+rowsData[0].id;
		createmodifytaskwindow(title,url,width,height);
	}

	function cancelOA(taskId,leaveType,reason){
		taskComplete(taskId,[{
			key: 'reApply',
			value: false,
			type: 'B'
		},{
			key: 'leaveType',
			value: leaveType,
			type: 'S'
		},{
			key: 'reason',
			value: reason,
			type: 'S'
		}],'取消申请','你确定要取消此请假流程吗?');
	}

	/**
	 * 创建工作流程的处理页面
	 * @param title
	 * @param addurl
	 * @param saveurl
	 */
	function createmodifytaskwindow(title,addurl,width,height) {
		width = width?width:700;
		height = height?height:400;
		if(width=="100%" || height=="100%"){
			width = document.body.offsetWidth;
			height =document.body.offsetHeight-100;
		}
		$.dialog({
			content: 'url:'+addurl,
			lock : true,
			width:width,
			height:height,
			title:title,
			opacity : 0.3,
			cache:false,
		    button:[{
		    	name :'提交',
		    	callback : function(){
		    		iframe = this.iframe.contentWindow.document;
		    		var taskId = $('#taskId',iframe).val();
		    		// 设置流程变量
		    		taskComplete(taskId, [{
						key: 'reApply',
						value: true,
						type: 'B'
					},{
						key: 'leaveType',
						value: $('#leaveType',iframe).val(),
						type: 'S'
					},{
						key: 'reason',
						value: $('#reason',iframe).val(),
						type: 'S'
					}],'请假流程修改','确定要修改后提交吗?');
					return false;
		    	}
		    }],
		    cancelVal: '关闭',
		    cancel: true /*为true等价于function(){}*/
		});
	};
</script>
 
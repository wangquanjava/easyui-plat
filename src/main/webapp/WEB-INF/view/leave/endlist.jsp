<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<div class="easyui-layout" fit="true" id="system_function_functionList">
  <div region="center" style="padding:1px;">
<t:datagrid name="endList" title="已结束流程" actionUrl="leaveController.do?finishedList" idField="id" sortName="id" sortOrder="desc">
   <t:dgCol title="id" field="id" hidden="false" align="center"></t:dgCol>
   <t:dgCol title="流程ID" field="processInstanceId" width="40" align="center"></t:dgCol>
   <t:dgCol title="请假事由" field="reason" width="80" align="center"></t:dgCol>
   <t:dgCol title="类型" field="leaveType" width="40" dictionary="oa_type" align="center"></t:dgCol>
   <t:dgCol title="开始时间" field="startTime" width="120" align="center"></t:dgCol>
   <t:dgCol title="结束时间" field="endTime"  width="120" align="center"></t:dgCol>
   <t:dgCol title="实际开始时间" field="realityStartTime" width="120" align="center"></t:dgCol>
   <t:dgCol title="实际结束时间" field="realityEndTime" width="120" align="center"></t:dgCol>
   <t:dgCol title="申请时间" field="applyTime" width="120" align="center"></t:dgCol>
   <t:dgToolBar title="查看" icon="icon-search" url="leaveController.do?toEdit&root=leave" funname="detail" height="350" width="700"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
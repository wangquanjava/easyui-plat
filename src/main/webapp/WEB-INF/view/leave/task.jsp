<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>请假申请</title>
  <t:base type="jq,easyui,tools,DatePicker"></t:base>
 </head>
 <body style="overflow-y: auto">
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="${pageContext.request.contextPath }/leaveController.do?complete">
			<input id="id" name="id" type="hidden" value="${entity.id }">
			<input id="userId" name="userId" type="hidden" value="${entity.userId }">
			<input id="taskId" name="taskId" type="hidden" value="${src.taskId }">
			<table cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right" width="25%">
						<label class="Validform_label">
							请假类型:
						</label>
					</td>
					<td class="value" width="75%">
						<t:dictSelect field="leaveType" typeGroupCode="oa_type" hasLabel="false" defaultVal="${entity.leaveType}"></t:dictSelect>
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							开始时间:
						</label>
					</td>
					<td class="value">
						<input class="inputxt Wdate" id="reason" name="startTime"
							   value="${entity.startTime}" datatype="*" onfocus="WdatePicker({isShowWeek:true})" readonly="readonly">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							事由:
						</label>
					</td>
					<td class="value">
						<input class="inputxt" id="reason" name="reason"
							   value="${entity.reason}" datatype="*">
						<span class="Validform_checktip"></span>
					</td>
				</tr>
			</table>
		</t:formvalid>
 </body>
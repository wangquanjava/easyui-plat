<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="/easyui-tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>工作流程业务演示系统</title>
<t:base type="jq,easyui,tools,DatePicker"/>

<script type="text/javascript">
$(function() {
	
	/*$('#layout_jeecg_onlineDatagrid').datagrid({
		url : 'http://127.0.0.1:8888/ea/userController.do?list&field=id,userName,userAccount',
		title : '',
		iconCls : '',
		fit : true,
		fitColumns : true,
		pagination : true,
		pageSize : 10,
		pageList : [ 10 ],
		nowarp : false,
		border : false,
		idField : 'id',
		sortName : 'id',
		sortOrder : 'desc',
		frozenColumns : [ [ {
			title : '编号',
			field : 'id',
			width : 150,
			hidden : true
		} ] ],
		columns : [ [ {
			title : '登录账号',
			field : 'userAccount',
			width : 100,
			align : 'center',
			sortable : true,
			formatter : function(value, rowData, rowIndex) {
				return formatString('<span title="{0}">{1}</span>', value, value);
			}
		}, {
			title : '登陆名',
			field : 'userName',
			width : 150,
			align : 'center',
			sortable : true,
			formatter : function(value, rowData, rowIndex) {
				return formatString('<span title="{0}">{1}</span>', value, value);
			}
		}] ],
		onClickRow : function(rowIndex, rowData) {
		},
		onLoadSuccess : function(data) {
			$('#layout_jeecg_onlinePanel').panel('setTitle', '( ' + data.total + ' )人在线');
		}
	}).datagrid('getPager').pagination({
		showPageList : false,
		showRefresh : false,
		beforePageText : '',
		afterPageText : '/{pages}',
		displayMsg : ''
	});		
	*/
	$('#layout_east_calendar').calendar({
		fit : true,
		current : new Date(),
		border : false,
		onSelect : function(date) {
			$(this).calendar('moveTo', new Date());
		}
	});
	
	window.setInterval(function() {
		$('#layout_jeecg_onlineDatagrid').datagrid('load', {});
	}, 1000 * 60 * 10);
	
	$("#layout_jeecg_onlinePanel .datagrid-view .datagrid-view2 .datagrid-body").css("max-height","180px").css("overflow-y","auto");
	
	$(".layout-expand").click(function(){
		$('#layout_east_calendar').css("width","auto");
		$('#layout_east_calendar').parent().css("width","auto");
		$("#layout_jeecg_onlinePanel").find(".datagrid-view").css("max-height","200px");
		$("#layout_jeecg_onlinePanel .datagrid-view .datagrid-view2 .datagrid-body").css("max-height","180px").css("overflow-y","auto");
	});
});

</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',split:true,border:false" title="SPRING AND EASYUI DEMO" style="height: 85px; padding: 1px; overflow: hidden;">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="vertical-align:text-bottom"></td>
				<td align="right" nowrap>
					<table>
						<tr>
						<td>
							<div>
								<span style="color: #CC33FF">当前用户:</span><span style="color: red">【${sessionScope['SPRING_SECURITY_LAST_USERNAME']}】<span style="color: #CC33FF">职务</span>:<span style="color: #666633">${roleName }</span>
							</div>
							<div style="position: absolute; right: 20px; bottom: 1px;">
								<a href="javascript:void(0);" class="easyui-menubutton" menu="#changetheme" iconCls="icon-change" style="width: 86px;">切换皮肤</a>
								<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_kzmbMenu" iconCls="icon-control" style="width: 86px;">控制面板</a>
								<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_zxMenu" iconCls="icon-logout" style="width: 86px;">注销登陆</a>
							</div>
							<div id="layout_north_kzmbMenu" style="width: 85px; display: none;">
								<div onclick="openwindow('用户信息','userController.do?userinfo')">个人信息</div>
								<div class="menu-sep"></div>
								<div onclick="add('修改密码','userController.do?changepassword')">修改密码</div>
								
						        <!-- <div class="menu-sep"></div> -->
						        <!-- <div onclick="add('修改首页风格','indexController.do?toListPage&pathroot=user&page=changetheme')">首页风格 </div> -->
							</div>
							<div id="changetheme" style="width: 120px; display: none;">
									<div onclick="changeThemeFun('default');" iconCls='icon-menu'>默认</div>
									<div onclick="changeThemeFun('green');" iconCls='icon-menu'>绿色</div>
									<div class="menu-sep"></div>
									<div onclick="changeThemeFun('gray');" iconCls='icon-menu'>灰色</div>
									<div onclick="changeThemeFun('pink');" iconCls='icon-menu'>红</div>
									<div class="menu-sep"></div>
									<div onclick="changeThemeFun('sunny');" iconCls='icon-menu'>橘黄</div>
									<div onclick="changeThemeFun('metro');" iconCls='icon-menu'>Metro</div>
									<div onclick="changeThemeFun('cupertino');" iconCls='icon-menu'>Cupertino</div>
									<div class="menu-sep"></div>
									<div onclick="changeThemeFun('dark-hive');" iconCls='icon-menu'>Dark-Hive</div>
									<div onclick="changeThemeFun('pepper-grinder');" iconCls='icon-menu'>Pepper-Grinder</div>
							</div>
							<div id="layout_north_zxMenu" style="width: 85px; display: none;">
								<div class="menu-sep"></div>
								<div onclick="exit('j_spring_security_logout','确定退出该系统吗 ?',1);">退出系统</div>
							</div></td>
						</tr>
					</table></td>
				<td align="right">&nbsp;&nbsp;&nbsp;</td>
			</tr>
		</table>
	</div>
	<%--<div data-options="region:'south',split:true" style="height:80px;padding:1px;background:#efefef;">
		<div class="easyui-layout" data-options="fit:true" style="background:#ccc;">
			<div data-options="region:'center'">QQ:123456789</div>
		</div>
	</div>--%>
	<div collapsed="true" data-options="region:'east',iconCls:'icon-reload',split:true" title="辅助工具" style="width:180px;">
		<div id="tabs" class="easyui-tabs" border="false" style="height: 240px">
			<div title="日历查看" style="padding: 0px; overflow: hidden; color: red;">
				<div id="layout_east_calendar"></div>
			</div>
		</div>
		<div id="layout_jeecg_onlinePanel" data-options="fit:true,border:false" title="用户在线列表">
			<table id="layout_jeecg_onlineDatagrid"></table>
		</div>
	</div>
	<div data-options="region:'west',split:true" title="导航菜单" href="indexController.do?left" style="width:150px;padding1:1px;overflow:hidden;">
	</div>
	<div data-options="region:'center'" style="overflow:hidden;">
		<div class="easyui-tabs" data-options="fit:true,border:false" id="main_tab">
			<div title="首页" style="padding:20px;overflow:hidden;"> 
				<div style="margin-top:20px;font-size: 24px;">
				</div>
			</div>
		</div>
	</div>
    <div id="mm" class="easyui-menu" style="width:150px;background:#fafafa;font-size: 14px">
        <div id="mm-tabupdate">刷新</div>
        <div class="menu-sep"></div>
        <div id="mm-tabclose">关闭</div>
        <div id="mm-tabcloseall">全部关闭</div>
        <div id="mm-tabcloseother">除此之外全部关闭</div>
        <div class="menu-sep"></div>
        <div id="mm-tabcloseright">当前页右侧全部关闭</div>
        <div id="mm-tabcloseleft">当前页左侧全部关闭</div>
        <div class="menu-sep"></div>
        <div id="mm-exit">退出</div>
    </div>
</body>
</html>
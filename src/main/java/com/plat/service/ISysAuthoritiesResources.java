package com.plat.service;

import com.plat.security.entity.SysAuthoritiesResources;


public interface ISysAuthoritiesResources extends IBaseService<SysAuthoritiesResources>{

    public void updateSysAuthoritiesResources(String rolefunctions, String authorityId) throws Exception;
}

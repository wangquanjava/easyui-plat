package com.plat.service;

import java.util.List;
import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

import com.plat.security.entity.Leave;
import com.plat.tag.core.json.DataGrid;

public interface ILeave extends IBaseService<Leave> {

	/**
	 * 请假流程启动
	 * @param entity
	 * @param variables
	 * @return
	 */
	public ProcessInstance saveWorkflow(Leave entity, Map<String, Object> variables);

	/**
	 * 根据角色查询要处理的业务审批流程
	 * @param userId
	 * @param dataGrid
	 * @param groups
	 * @return
	 */
	public List<Leave> findTodoTasks(String userId, DataGrid dataGrid, List<String> groups);

	/**
	 * 根据角色查询已经启动的业务审批流程
	 * @param dataGrid
	 * @param groups
	 * @return
	 */
	public List<Leave> findRunningProcessInstaces(DataGrid dataGrid, List<String> groups);

	/**
	 * 已经结束的请假流程
	 * @return
	 */
	public List<Leave> findFinishedProcessInstaces();
}

package com.plat.service;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.plat.core.hibernate.qbc.CriteriaQuery;
import com.plat.security.entity.SysUsers;



public interface ISysUsersService extends IBaseService<SysUsers>{

	Collection<GrantedAuthority> loadUserAuthoritiesByName(String username);

	SysUsers findByUserAccount(String username);


}

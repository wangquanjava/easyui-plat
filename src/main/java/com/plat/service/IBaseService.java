package com.plat.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;

import com.plat.core.hibernate.qbc.CriteriaQuery;
import com.plat.tag.core.easyui.ComboTreeModel;
import com.plat.tag.core.easyui.TreeGridModel;
import com.plat.tag.core.json.ComboTree;
import com.plat.tag.core.json.DataGridReturn;
import com.plat.tag.core.json.TreeGrid;

public interface IBaseService<T> {

    /**
     * <br>
     * <b>功能：</b>获取单个对象<br>
     * <b>作者：</b>wench<br>
     * <b>日期：</b> 2012-7-29 <br>
     * @param entityClass
     * @param id
     * @return
     */
    public T get(Serializable id);

    /**
     * <br>
     * <b>功能：</b>获取所有对象<br>
     * <b>作者：</b>wench<br>
     * <b>日期：</b> 2012-7-29 <br>
     * @param entityClass
     * @return
     */
    public List<T> getAll();

    /**
     * 保存对象.
     */
    public void save(Object o);

    /**
     * 删除对象.
     */
    public void remove(Object o);

    /**
     * 根据ID删除对象.
     */
    public void removeById(Serializable id);

    DataGridReturn getDataGridReturn(CriteriaQuery cq, boolean b);

    public void updateMerge(Object o);

    public List<T> findBy(String propertyName, Object value);

    public List<T> find(String hql, Object... values);

    public Criteria createCriteria(Criterion... criterions);

    public Query createQuery(String hql, Object... values);

    /**
     * 根据模型生成JSON
     * @param all 全部对象
     * @param in 已拥有的对象
     * @param comboBox 模型
     * @return
     */
    public List<ComboTree> comboTree(List all, ComboTreeModel comboTreeModel, List in);
    
    public  List<TreeGrid> treegrid(List all,TreeGridModel treeGridModel);
}

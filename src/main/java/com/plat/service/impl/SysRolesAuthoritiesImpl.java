package com.plat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plat.dao.IBaseDao;
import com.plat.security.dao.SysRolesAuthoritiesDao;
import com.plat.security.entity.SysRolesAuthorities;
import com.plat.service.ISysRolesAuthorities;

@Service
public class SysRolesAuthoritiesImpl extends BaseServiceImpl<SysRolesAuthorities> implements ISysRolesAuthorities {

    @Autowired
    private SysRolesAuthoritiesDao dao;

    @Override
    public IBaseDao<SysRolesAuthorities> getDao() {
        return dao;
    }

}

package com.plat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plat.dao.IBaseDao;
import com.plat.security.dao.SysUsersRolesDao;
import com.plat.security.entity.SysUsersRoles;
import com.plat.service.ISysUsersRoles;

@Service
public class SysUsersRolesImpl extends BaseServiceImpl<SysUsersRoles> implements ISysUsersRoles {

    @Autowired
    private SysUsersRolesDao dao;

    @Override
    public IBaseDao<SysUsersRoles> getDao() {
        return dao;
    }

}

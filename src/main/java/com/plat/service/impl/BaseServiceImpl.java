package com.plat.service.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;

import com.plat.core.hibernate.qbc.CriteriaQuery;
import com.plat.dao.IBaseDao;
import com.plat.service.IBaseService;
import com.plat.tag.core.easyui.ComboTreeModel;
import com.plat.tag.core.easyui.TreeGridModel;
import com.plat.tag.core.json.ComboTree;
import com.plat.tag.core.json.DataGridReturn;
import com.plat.tag.core.json.TreeGrid;

public abstract class BaseServiceImpl<T> implements IBaseService<T> {

    public abstract IBaseDao<T> getDao();

    @Override
    public T get(Serializable id) {
        return getDao().get(id);
    }

    @Override
    public List<T> getAll() {
        return getDao().getAll();
    }

    @Override
    public void save(Object o) {
        getDao().save(o);
    }

    @Override
    public void remove(Object o) {
        getDao().remove(o);
    }

    @Override
    public void removeById(Serializable id) {
        getDao().removeById(id);
    }

    @Override
    public DataGridReturn getDataGridReturn(CriteriaQuery cq, boolean b) {
        return getDao().getDataGridReturn(cq, b);

    }

    @Override
    public void updateMerge(Object o) {
        getDao().updateMerge(o);
    }

    @Override
    public List<T> findBy(String propertyName, Object value) {
        return getDao().findBy(propertyName, value);
    }

    @Override
    public List<T> find(String hql, Object... values) {
        return getDao().find(hql, values);
    }

    @Override
    public Criteria createCriteria(Criterion... criterions) {
        return getDao().createCriteria(criterions);
    }

    @Override
    public Query createQuery(String hql, Object... values) {
        return getDao().createQuery(hql, values);
    }

    @Override
    public List<ComboTree> comboTree(List all, ComboTreeModel comboTreeModel, List in) {
        return getDao().comboTree(all, comboTreeModel, in);
    }
    
    @Override
    public List<TreeGrid> treegrid(List all, TreeGridModel treeGridModel) {
        return getDao().treegrid(all, treeGridModel);
    }
}

package com.plat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plat.dao.IBaseDao;
import com.plat.security.dao.DictionaryDao;
import com.plat.security.entity.DictionaryType;
import com.plat.service.IDictionaryService;

@Service
public class DictionaryServiceImpl extends BaseServiceImpl<DictionaryType> implements IDictionaryService{

    @Autowired
    private DictionaryDao dao;
    
    @Override
    public IBaseDao<DictionaryType> getDao() {
        return dao;
    }

}

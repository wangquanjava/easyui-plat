package com.plat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plat.dao.IBaseDao;
import com.plat.security.dao.SysAuthoritiesDao;
import com.plat.security.entity.SysAuthorities;
import com.plat.service.ISysAuthorities;

@Service
public class SysAuthoritiesImpl extends BaseServiceImpl<SysAuthorities> implements ISysAuthorities{

	@Autowired
	private SysAuthoritiesDao dao;
	
	@Override
	public IBaseDao<SysAuthorities> getDao() {
		return dao;
	}

}

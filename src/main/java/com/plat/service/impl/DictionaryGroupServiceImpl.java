package com.plat.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.plat.dao.IBaseDao;
import com.plat.security.dao.DictionaryDao;
import com.plat.security.dao.DictionaryGroupDao;
import com.plat.security.entity.DictionaryType;
import com.plat.security.entity.DictionaryTypeGroup;
import com.plat.service.IDictionaryGroupService;

@Service
public class DictionaryGroupServiceImpl extends BaseServiceImpl<DictionaryTypeGroup> implements IDictionaryGroupService {

    @Autowired
    private DictionaryGroupDao dao;

    @Autowired
    private DictionaryDao typeDao;

    @Override
    public IBaseDao<DictionaryTypeGroup> getDao() {
        return dao;
    }

    @Override
    public void initTypeGroups() {
        List<DictionaryTypeGroup> groups = getDao().getAll();
        for (DictionaryTypeGroup g : groups) {
            DictionaryTypeGroup.allTypeGroups.put(g.getTypegroupcode().toLowerCase(), g);
            List<DictionaryType> types = typeDao.findBy("TSTypegroup.typegroupid", g.getTypegroupid());
            DictionaryTypeGroup.allTypes.put(g.getTypegroupcode().toLowerCase(), types);
        }
    }

    public void refleshTypeGroupCach() {
        DictionaryTypeGroup.allTypeGroups.clear();
        List<DictionaryTypeGroup> typeGroups = getDao().getAll();
        for (DictionaryTypeGroup tsTypegroup : typeGroups) {
            DictionaryTypeGroup.allTypeGroups.put(tsTypegroup.getTypegroupcode().toLowerCase(), tsTypegroup);
        }
    }

    public void refleshTypesCach(DictionaryType type) {
        DictionaryTypeGroup tsTypegroup = type.getTSTypegroup();
        DictionaryTypeGroup typeGroupEntity = get(tsTypegroup.getId());
        List<DictionaryType> types = typeDao.findBy("TSTypegroup.typegroupid", typeGroupEntity.getTypegroupid());
        DictionaryTypeGroup.allTypes.put(typeGroupEntity.getTypegroupcode().toLowerCase(), types);
    }

    public void clearTypes(String typegroupcode) {
        Map<String, List<DictionaryType>> allTypes = DictionaryTypeGroup.allTypes;
        if (allTypes.get(typegroupcode) != null) {
            allTypes.remove(typegroupcode);
        }
    }
}

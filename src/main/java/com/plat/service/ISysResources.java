package com.plat.service;

import java.util.List;

import com.plat.security.entity.SysResources;

public interface ISysResources extends IBaseService<SysResources> {

	List<SysResources> findParentUrl(List<String> authNames);

    List<SysResources> findChildNodes(String pid,List<String> authNames);
}

package com.plat.service;

import com.plat.security.entity.DictionaryType;
import com.plat.security.entity.DictionaryTypeGroup;

public interface IDictionaryGroupService extends IBaseService<DictionaryTypeGroup> {

    public void initTypeGroups();

    public void refleshTypeGroupCach();

    public void refleshTypesCach(DictionaryType type);

    public void clearTypes(String typegroupcode);
}

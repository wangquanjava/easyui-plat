package com.plat.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.plat.security.entity.SysResources;
import com.plat.service.IBaseService;
import com.plat.service.ISysResources;
import com.plat.util.JsonUtil;

@Controller("indexController")
@RequestMapping("/indexController.do")
public class IndexController extends BaseController {

    @Autowired
    private ISysResources sysResources;

    @RequestMapping(params = "main")
    public ModelAndView main(HttpServletRequest request) {
        if (getSecurityContext(request) == null) {
            return new ModelAndView("redirect:/login.do");
        }
        return new ModelAndView("main");
    }

    @RequestMapping(params = "left")
    public ModelAndView left(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("left");
        List<SysResources> models = sysResources.findParentUrl(getSessionAuthorities(request));
        modelAndView.addObject("m", models);
        return modelAndView;
    }

    @RequestMapping(params = "findChrids")
    @ResponseBody
    public Object findChrids(HttpServletRequest request, String pid) {

        List<Map<String, Object>> lists = new ArrayList<Map<String, Object>>();
        Map<String, Object> m = null;
        List<SysResources> models = sysResources.findChildNodes(pid, getSessionAuthorities(request));

        for (SysResources o : models) {
            m = new HashMap<String, Object>();
            m.put("id", o.getResourceId());
            m.put("text", o.getResourceName());
            m.put("pid", o.getPid());
            m.put("state", "closed");
            m.put("iconCls", o.getIcon());
            m.put("attributes", "{\"url\":\"" + o.getResourceString() + "\",\"isLeaf\":\"" + o.getIsLeaf() + "\"}");
            lists.add(m);
        }

        return JsonUtil.buildArrayList(lists);
    }

    @Override
    public IBaseService getService() {
        // TODO Auto-generated method stub
        return null;
    }
}

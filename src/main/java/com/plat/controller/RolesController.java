package com.plat.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.plat.security.entity.SysRoles;
import com.plat.service.IBaseService;
import com.plat.service.ISysResources;
import com.plat.service.ISysRoles;

/**
 * @Title: Controller
 * @Description: 权限维护
 * @author wench
 * @date 2014-02-01 21:30:30
 * @version V1.0
 */
@Controller
@RequestMapping("/rolesController.do")
public class RolesController extends BaseController<SysRoles> {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(RolesController.class);

    @Autowired
    private ISysRoles service;

    @Autowired
    private ISysResources sysResources;

    @Override
    public IBaseService<SysRoles> getService() {
        return service;
    }

}

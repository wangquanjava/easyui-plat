package com.plat.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.plat.security.entity.SysAuthorities;
import com.plat.security.entity.SysAuthoritiesResources;
import com.plat.security.entity.SysResources;
import com.plat.service.IBaseService;
import com.plat.service.ISysAuthorities;
import com.plat.service.ISysAuthoritiesResources;
import com.plat.service.ISysResources;
import com.plat.tag.core.easyui.ComboTreeModel;
import com.plat.tag.core.json.AjaxJson;
import com.plat.tag.core.json.ComboTree;

@SuppressWarnings("unchecked")
@Controller
@RequestMapping("/authorsController.do")
public class AuthorsController extends BaseController<SysAuthorities> {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(AuthorsController.class);

    @Autowired
    private ISysAuthorities service;

    @Autowired
    private ISysResources sysResources;

    @Autowired
    private ISysAuthoritiesResources sysAuthoritiesResources;

    @Override
    public IBaseService<SysAuthorities> getService() {
        return service;
    }

    /**
     * 角色列表页面跳转
     * @return
     */
    @RequestMapping(params = "rolesFun")
    public ModelAndView rolesFun(String authorityId) {
        ModelAndView mv = new ModelAndView("roles/roleSet");
        mv.addObject("authorityId", authorityId);
        return mv;
    }

    @RequestMapping(params = "setAuthority")
    @ResponseBody
    public List<ComboTree> setAuthority(SysAuthorities role, ComboTree comboTree) {
        Criteria cri = sysResources.createCriteria();
        if (comboTree.getId() != null) {
            cri.add(Restrictions.eq("pid", comboTree.getId()));
        } else {
            cri.add(Restrictions.lt("level", 2));
        }
        List<SysResources> roots = cri.list();
        List<SysAuthoritiesResources> authoris = sysAuthoritiesResources.findBy("authorityId", role.getAuthorityId());//已有权限的菜单

        List<SysResources> inList = new ArrayList<SysResources>();

        //获取已有的权限
        for (SysAuthoritiesResources auth : authoris) {
            inList.add(auth.getSysResources());
        }

        ComboTreeModel comboTreeModel = new ComboTreeModel("resourceId", "resourceName", "childResources");
        List<ComboTree> comboTrees = service.comboTree(roots, comboTreeModel, inList);
        return comboTrees;
    }

    /**
     * 更新权限
     * @param request
     * @return
     */
    @RequestMapping(params = "updateAuthority")
    @ResponseBody
    public AjaxJson updateAuthority(String rolefunctions, String authorityId) {
        AjaxJson j = new AjaxJson();
        try {
            sysAuthoritiesResources.updateSysAuthoritiesResources(rolefunctions, authorityId);
            j.setMsg("权限更新成功");
        } catch (Exception e) {
            logger.error("权限更新失败", e);
            j.setSuccess(false);
            j.setMsg("权限更新失败");
        }
        return j;
    }
}

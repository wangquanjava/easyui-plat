package com.plat.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.plat.security.entity.SysUsers;
import com.plat.service.IBaseService;
import com.plat.service.ISysUsersService;
import com.plat.tag.core.json.AjaxJson;
import com.plat.tag.core.json.AjaxUtils;
import com.plat.util.MyBeanUtils;

/**
 * Created by Administrator on 14-1-26.
 */
@Controller("userController")
@RequestMapping(value = "/userController")
public class UserController extends BaseController<SysUsers> {

    private static final Logger logger = Logger.getLogger(UserController.class);

    @Autowired
    private ISysUsersService usersService;

    @Override
    public IBaseService<SysUsers> getService() {
        return usersService;
    }

    @RequestMapping(params = "doSaveUser")
    @ResponseBody
    public AjaxJson doSave(final SysUsers t) throws Exception {

        return new AjaxUtils() {
            @Override
            protected Object busCall() throws Exception {
                if (t.getId() != null) {
                    SysUsers entity = getService().get(t.getId());

                    MyBeanUtils.copyBeanNotNull2Bean(t, entity);
                    getService().updateMerge(t);
                } else {
                    getService().save(t);
                }
                return null;
            }
        }.excuteJson("操作成功!", "操作失败!");
    }
}

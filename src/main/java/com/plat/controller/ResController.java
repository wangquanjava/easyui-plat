package com.plat.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.plat.security.entity.SysResources;
import com.plat.service.IBaseService;
import com.plat.service.ISysResources;
import com.plat.tag.core.easyui.ComboTreeModel;
import com.plat.tag.core.easyui.TreeGridModel;
import com.plat.tag.core.json.ComboTree;
import com.plat.tag.core.json.TreeGrid;

@Controller("resController")
@RequestMapping(value = "/resController")
public class ResController extends BaseController<SysResources> {

    @Autowired
    private ISysResources service;

    @Override
    public IBaseService<SysResources> getService() {
        return service;
    }

    
    @RequestMapping(params = "functionGrid")
    @ResponseBody
    public List<TreeGrid> functionGrid(HttpServletRequest request, TreeGrid treegrid) {
        Criteria cri = getService().createCriteria();
        if (treegrid.getId() != null) {
            String pid = getService().get(Long.valueOf(treegrid.getId())).getResourceId();
            cri.add(Restrictions.eq("pid", pid));
        }
        if (treegrid.getId() == null) {
            cri.add(Restrictions.eq("level", 1));
        }
        List<SysResources> roots = cri.list();
        
        TreeGridModel treeGridModel = new TreeGridModel();
        treeGridModel.setIcon("icon");
        treeGridModel.setTextField("resourceName");
        treeGridModel.setParentText("parentResources_resourceName");
        treeGridModel.setParentId("pid");
        treeGridModel.setSrc("resourceString");
        treeGridModel.setIdField("id");
        treeGridModel.setChildList("childResources");
        //添加排序字段
        treeGridModel.setOrder("rsort");
        List<TreeGrid> treeGrids = getService().treegrid(roots, treeGridModel);
        
        return treeGrids;
    }
    
    /**
     * 父级权限下拉菜单
     */
    @RequestMapping(params = "getParent")
    @ResponseBody
    public List<ComboTree> getParent(HttpServletRequest request, ComboTree comboTree) {
        Criteria cri = getService().createCriteria();
        if (comboTree.getId() != null) {
            cri.add(Restrictions.eq("pid", comboTree.getId()));
        } else {
            cri.add(Restrictions.lt("level", 2));
        }
        List<SysResources> roots = cri.list();

        ComboTreeModel comboTreeModel = new ComboTreeModel("resourceId", "resourceName", "childResources");
        List<ComboTree> comboTrees = service.comboTree(roots, comboTreeModel,null);
        return comboTrees;
    }
}

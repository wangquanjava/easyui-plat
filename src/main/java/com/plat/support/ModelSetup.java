package com.plat.support;

import java.util.Map;

/**
 * DESC: build data interface
 * @author JETYOU@FOXMAIL.COM
 * @version V1.0 ${date}
 * @since iFRAME
 */
public interface ModelSetup {

    public void setup(Map<String, Object> params);

}

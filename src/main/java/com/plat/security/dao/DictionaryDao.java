package com.plat.security.dao;

import org.springframework.stereotype.Repository;

import com.plat.dao.IBaseDao;
import com.plat.dao.impl.BaseDaoImpl;
import com.plat.security.entity.DictionaryType;

@Repository
public class DictionaryDao extends BaseDaoImpl<DictionaryType> implements IBaseDao<DictionaryType>{

}

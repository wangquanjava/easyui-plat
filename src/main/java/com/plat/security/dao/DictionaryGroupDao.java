package com.plat.security.dao;

import org.springframework.stereotype.Repository;

import com.plat.dao.IBaseDao;
import com.plat.dao.impl.BaseDaoImpl;
import com.plat.security.entity.DictionaryTypeGroup;

@Repository
public class DictionaryGroupDao extends BaseDaoImpl<DictionaryTypeGroup> implements IBaseDao<DictionaryTypeGroup>{

}

package com.plat.security.dao;

import org.springframework.stereotype.Repository;

import com.plat.dao.IBaseDao;
import com.plat.dao.impl.BaseDaoImpl;
import com.plat.security.entity.Leave;

@Repository
public class LeaveDao extends BaseDaoImpl<Leave> implements IBaseDao<Leave>{

}

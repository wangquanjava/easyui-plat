/*
 * @(#) MyUserDetailsService.java 2011-3-23 上午09:04:31
 * Copyright 2011 by Sparta
 */

package com.plat.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.plat.security.entity.SysUsers;
import com.plat.service.ISysAuthoritiesResources;
import com.plat.service.ISysUsersService;
import com.plat.util.SpringApplicationContextHolder;

/**
 * 该类的主要作用是为Spring Security提供一个经过用户认证后的UserDetails。 该UserDetails包括用户名、密码、是否可用、是否过期等信息。 sparta 11/3/29
 */
@Service("userDetailsManager")
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private ISysUsersService sysUsersDao;

	@Autowired
	private ISysAuthoritiesResources pubAuthoritiesResourcesDao;

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	@Autowired
	private UserCache userCache;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {

		Collection<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();

		// if (null == sysUsersDao) {
		// sysUsersDao = new SysUsersDao();
		// }

		// 得到用户的权限
		auths = sysUsersDao.loadUserAuthoritiesByName(username);

		// 根据用户名取得一个SysUsers对象，以获取该用户的其他信息。
		SysUsers user = sysUsersDao.findByUserAccount(username);
		
		ApplicationContext context = SpringApplicationContextHolder.getApplicationContext();
		
		//刷新用户权限资源
		CustomInvocationSecurityMetadataSourceService customService = (CustomInvocationSecurityMetadataSourceService)context.getBean("customSecurityMetadataSource");
		customService.loadResourceDefine();
		
		return new SysUsers(user.getUserId(), user.getUserAccount(), user.getUserName(), user.getUserPassword(),
				user.getUserDesc(), true, false, user.getUserDuty(), user.getUserDept(), user.getSubSystem(),
				new HashSet(0), true, true, true, auths);
	}

	// set DataSource
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	// 设置用户缓存功能。
	public void setUserCache(UserCache userCache) {
		this.userCache = userCache;
	}

	public UserCache getUserCache() {
		return this.userCache;
	}

}

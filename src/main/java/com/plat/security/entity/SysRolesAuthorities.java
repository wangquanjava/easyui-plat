package com.plat.security.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "SYS_ROLES_AUTHORITIES")
public class SysRolesAuthorities extends IdEntity<Long> implements java.io.Serializable {

    @ManyToOne(optional = true)
    @JoinColumn(name = "AUTHORITY_ID", insertable = false, updatable = false, referencedColumnName = "AUTHORITY_ID")
    private SysAuthorities sysAuthorities;

    @ManyToOne(optional = true)
    @JoinColumn(name = "ROLE_ID", insertable = false, updatable = false, referencedColumnName = "ROLE_ID")
    private SysRoles sysRoles;

    @Column(name = "ROLE_ID")
    private String roleId;

    @Column(name = "AUTHORITY_ID")
    private String authorityId;

    @Column(name = "ENABLED")
    private Boolean enabled;

    public SysRolesAuthorities() {
    }

    public SysRolesAuthorities(Long id) {
        setId(id);
    }

    public SysRolesAuthorities(long id, SysAuthorities sysAuthorities, SysRoles sysRoles, Boolean enabled) {
        setId(id);
        this.sysAuthorities = sysAuthorities;
        this.sysRoles = sysRoles;
        this.enabled = enabled;
    }

    public SysAuthorities getSysAuthorities() {
        return this.sysAuthorities;
    }

    public void setSysAuthorities(SysAuthorities sysAuthorities) {
        this.sysAuthorities = sysAuthorities;
    }

    public SysRoles getSysRoles() {
        return this.sysRoles;
    }

    public void setSysRoles(SysRoles sysRoles) {
        this.sysRoles = sysRoles;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(String authorityId) {
        this.authorityId = authorityId;
    }

}

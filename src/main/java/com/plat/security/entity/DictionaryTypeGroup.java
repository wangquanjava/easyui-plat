package com.plat.security.entity;

// default package

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sys_type_group_t")
public class DictionaryTypeGroup extends IdEntity<Long> implements java.io.Serializable {

    public static Map<String, DictionaryTypeGroup> allTypeGroups = new HashMap<String, DictionaryTypeGroup>();

    public static Map<String, List<DictionaryType>> allTypes = new HashMap<String, List<DictionaryType>>();

    @Column(name = "typegroupname", length = 50)
    private String typegroupname;

    @Column(name = "typegroupcode", length = 50)
    private String typegroupcode;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "TSTypegroup",cascade=CascadeType.REMOVE)
    private List<DictionaryType> TSTypes = new ArrayList<DictionaryType>();

    @Column(name = "typegroupid", length = 11)
    private Long typegroupid;

    public String getTypegroupname() {
        return this.typegroupname;
    }

    public void setTypegroupname(String typegroupname) {
        this.typegroupname = typegroupname;
    }

    public String getTypegroupcode() {
        return this.typegroupcode;
    }

    public void setTypegroupcode(String typegroupcode) {
        this.typegroupcode = typegroupcode;
    }

    public List<DictionaryType> getTSTypes() {
        return this.TSTypes;
    }

    public void setTSTypes(List<DictionaryType> TSTypes) {
        this.TSTypes = TSTypes;
    }

    public Long getTypegroupid() {
        return typegroupid;
    }

    public void setTypegroupid(Long typegroupid) {
        this.typegroupid = typegroupid;
    }

}

package com.plat.security.entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "SYS_RESOURCES")
public class SysResources extends IdEntity<Long> implements java.io.Serializable {

    @Column(name = "PID")
    private String pid;

    @Column(name = "LEVEL")
    private Integer level;

    @Column(name = "RSORT")
    private Long rsort;

    @Column(name = "RESOURCE_ID")
    private String resourceId;

    @Column(name = "RESOURCE_NAME")
    private String resourceName;

    @Column(name = "RESOURCE_DESC")
    private String resourceDesc;

    @Column(name = "RESOURCE_TYPE")
    private String resourceType;

    @Column(name = "RESOURCE_STRING")
    private String resourceString;

    @Column(name = "PRIORITY")
    private Boolean priority;

    // 是否可用，0为不可用，1为可用
    @Column(name = "ENABLED")
    private Integer enabled;

    // 是否是超级。0为不超级，1为超级。
    @Column(name = "ISSYS")
    private Integer issys;

    @Column(name = "MODULE")
    private String module;

    @Column(name = "is_Leaf")
    private String isLeaf;

    @Column(name = "icon")
    private String icon;

    @OneToMany(mappedBy = "sysResources", fetch = FetchType.LAZY)
    private Set<SysAuthoritiesResources> pubAuthoritiesResourceses = new HashSet<SysAuthoritiesResources>(0);

    /**
     * 父菜单
     */
    @ManyToOne
    @JoinColumn(name = "PID", insertable = false, updatable = false, referencedColumnName = "RESOURCE_ID")
    private SysResources parentResources;

    /**
     * 子菜单
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentResources")
    private List<SysResources> childResources = new ArrayList<SysResources>();

    public SysResources() {
    }

    public SysResources(String resourceId) {
        this.resourceId = resourceId;
    }

    public SysResources(String resourceId, String resourceName, String resourceDesc, String resourceType, String resourceString, Boolean priority, Integer enabled, Integer issys, String module,
            Set pubAuthoritiesResourceses) {
        this.resourceId = resourceId;
        this.resourceName = resourceName;
        this.resourceDesc = resourceDesc;
        this.resourceType = resourceType;
        this.resourceString = resourceString;
        this.priority = priority;
        this.enabled = enabled;
        this.issys = issys;
        this.module = module;
        this.pubAuthoritiesResourceses = pubAuthoritiesResourceses;
    }

    public String getResourceId() {
        return this.resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceName() {
        return this.resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getResourceDesc() {
        return this.resourceDesc;
    }

    public void setResourceDesc(String resourceDesc) {
        this.resourceDesc = resourceDesc;
    }

    public String getResourceType() {
        return this.resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceString() {
        return this.resourceString;
    }

    public void setResourceString(String resourceString) {
        this.resourceString = resourceString;
    }

    public Boolean getPriority() {
        return this.priority;
    }

    public void setPriority(Boolean priority) {
        this.priority = priority;
    }

    public Integer getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public Integer getIssys() {
        return this.issys;
    }

    public void setIssys(Integer issys) {
        this.issys = issys;
    }

    public String getModule() {
        return this.module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Set getSysAuthoritiesResourceses() {
        return this.pubAuthoritiesResourceses;
    }

    public void setSysAuthoritiesResourceses(Set pubAuthoritiesResourceses) {
        this.pubAuthoritiesResourceses = pubAuthoritiesResourceses;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getRsort() {
        return rsort;
    }

    public void setRsort(Long rsort) {
        this.rsort = rsort;
    }

    public String getIsLeaf() {
        return isLeaf;
    }

    public void setIsLeaf(String isLeaf) {
        this.isLeaf = isLeaf;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public SysResources getParentResources() {
        return parentResources;
    }

    public void setParentResources(SysResources parentResources) {
        this.parentResources = parentResources;
    }

    public List<SysResources> getChildResources() {
        return childResources;
    }

    public void setChildResources(List<SysResources> childResources) {
        this.childResources = childResources;
    }

    public Set<SysAuthoritiesResources> getPubAuthoritiesResourceses() {
        return pubAuthoritiesResourceses;
    }

    public void setPubAuthoritiesResourceses(Set<SysAuthoritiesResources> pubAuthoritiesResourceses) {
        this.pubAuthoritiesResourceses = pubAuthoritiesResourceses;
    }

}

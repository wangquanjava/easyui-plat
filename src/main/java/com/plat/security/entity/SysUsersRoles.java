package com.plat.security.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "SYS_USERS_ROLES")
public class SysUsersRoles extends IdEntity<Long> implements java.io.Serializable {

    @Column(name = "USER_ID")
    private String userId;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", insertable = false, updatable = false,referencedColumnName="USER_ID")
    private SysUsers pubUsers;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "ROLE_ID", insertable = false, updatable = false,referencedColumnName="ROLE_ID")
    private SysRoles pubRoles;

    @Column(name = "ROLE_ID")
    private String roleId;

    @Column(name = "enabled")
    private Boolean enabled;

    public SysUsersRoles() {
    }

    public SysUsersRoles(Long id) {
        setId(id);
    }

    public SysUsersRoles(Long id, SysUsers pubUsers, SysRoles pubRoles, Boolean enabled) {
        setId(id);
        this.pubUsers = pubUsers;
        this.pubRoles = pubRoles;
        this.enabled = enabled;
    }

    public SysUsers getSysUsers() {
        return this.pubUsers;
    }

    public void setSysUsers(SysUsers pubUsers) {
        this.pubUsers = pubUsers;
    }

    public SysRoles getSysRoles() {
        return this.pubRoles;
    }

    public void setSysRoles(SysRoles pubRoles) {
        this.pubRoles = pubRoles;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

}

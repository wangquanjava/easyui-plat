package com.plat.security.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "SYS_AUTHORITIES")
public class SysAuthorities extends IdEntity<Long> implements java.io.Serializable {

    @Column(name = "AUTHORITY_ID")
    private String authorityId;

    @Column(name = "AUTHORITY_NAME")
    private String authorityName;

    @Column(name = "AUTHORITY_DESC")
    private String authorityDesc;

    @Column(name = "ENABLED")
    private Boolean enabled;

    @Column(name = "issys")
    private Boolean issys;

    @Column(name = "module")
    private String module;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sysAuthorities")
    private Set<SysRolesAuthorities> pubRolesAuthoritieses = new HashSet<SysRolesAuthorities>(0);

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sysAuthorities")
    private Set<SysAuthoritiesResources> pubAuthoritiesResourceses = new HashSet<SysAuthoritiesResources>(0);

    public SysAuthorities() {
    }

    public SysAuthorities(String authorityId) {
        this.authorityId = authorityId;
    }

    public SysAuthorities(String authorityId, String authorityName, String authorityDesc, Boolean enabled, Boolean issys, String module, Set<SysRolesAuthorities> pubRolesAuthoritieses,
            Set<SysAuthoritiesResources> pubAuthoritiesResourceses) {
        this.authorityId = authorityId;
        this.authorityName = authorityName;
        this.authorityDesc = authorityDesc;
        this.enabled = enabled;
        this.issys = issys;
        this.module = module;
        this.pubRolesAuthoritieses = pubRolesAuthoritieses;
        this.pubAuthoritiesResourceses = pubAuthoritiesResourceses;
    }

    public String getAuthorityId() {
        return this.authorityId;
    }

    public void setAuthorityId(String authorityId) {
        this.authorityId = authorityId;
    }

    public String getAuthorityName() {
        return this.authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }

    public String getAuthorityDesc() {
        return this.authorityDesc;
    }

    public void setAuthorityDesc(String authorityDesc) {
        this.authorityDesc = authorityDesc;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getIssys() {
        return this.issys;
    }

    public void setIssys(Boolean issys) {
        this.issys = issys;
    }

    public String getModule() {
        return this.module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Set<SysRolesAuthorities> getPubRolesAuthoritieses() {
        return pubRolesAuthoritieses;
    }

    public void setPubRolesAuthoritieses(Set<SysRolesAuthorities> pubRolesAuthoritieses) {
        this.pubRolesAuthoritieses = pubRolesAuthoritieses;
    }

    public Set<SysAuthoritiesResources> getPubAuthoritiesResourceses() {
        return pubAuthoritiesResourceses;
    }

    public void setPubAuthoritiesResourceses(Set<SysAuthoritiesResources> pubAuthoritiesResourceses) {
        this.pubAuthoritiesResourceses = pubAuthoritiesResourceses;
    }

}

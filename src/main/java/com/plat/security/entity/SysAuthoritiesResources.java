package com.plat.security.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "SYS_AUTHORITIES_RESOURCES")
public class SysAuthoritiesResources implements java.io.Serializable {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "AUTHORITY_ID")
    private String authorityId;

    @Column(name = "RESOURCE_ID")
    private String resourceId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AUTHORITY_ID", insertable = false, updatable = false, referencedColumnName = "AUTHORITY_ID")
    private SysAuthorities sysAuthorities;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RESOURCE_ID", insertable = false, updatable = false, referencedColumnName = "RESOURCE_ID")
    private SysResources sysResources;

    @Column(name = "ENABLED")
    private Boolean enabled;

    public SysAuthoritiesResources() {
    }

    public SysAuthoritiesResources(Long id) {
        this.id = id;
    }

    public SysAuthoritiesResources(Long id, SysAuthorities sysAuthorities, SysResources sysResources, Boolean enabled) {
        this.id = id;
        this.sysAuthorities = sysAuthorities;
        this.sysResources = sysResources;
        this.enabled = enabled;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SysAuthorities getSysAuthorities() {
        return this.sysAuthorities;
    }

    public void setSysAuthorities(SysAuthorities sysAuthorities) {
        this.sysAuthorities = sysAuthorities;
    }

    public SysResources getSysResources() {
        return this.sysResources;
    }

    public void setSysResources(SysResources sysResources) {
        this.sysResources = sysResources;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getAuthorityId() {
        return authorityId;
    }

    public void setAuthorityId(String authorityId) {
        this.authorityId = authorityId;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

}

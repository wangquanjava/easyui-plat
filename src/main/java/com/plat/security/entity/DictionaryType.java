package com.plat.security.entity;

// default package

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 通用类型字典表
 */

@Entity
@Table(name = "sys_type_t")
public class DictionaryType extends IdEntity<Long> implements java.io.Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "typegroupid", referencedColumnName = "typegroupid")
    private DictionaryTypeGroup TSTypegroup;//类型分组

    @Column(name = "typegroupid", insertable = false, updatable = false)
    private Long typegroupid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "typepid", referencedColumnName = "typeid")
    private DictionaryType TSType;//父类型

    @Column(name = "typename", length = 50)
    private String typename;//类型名称

    @Column(name = "typecode", length = 50)
    private String typecode;//类型编码

    @Column(name = "typeid", length = 11)
    private Long typeid;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "TSType")
    private List<DictionaryType> TSTypes = new ArrayList();

    public DictionaryTypeGroup getTSTypegroup() {
        return this.TSTypegroup;
    }

    public void setTSTypegroup(DictionaryTypeGroup TSTypegroup) {
        this.TSTypegroup = TSTypegroup;
    }

    public DictionaryType getTSType() {
        return this.TSType;
    }

    public void setTSType(DictionaryType TSType) {
        this.TSType = TSType;
    }

    public String getTypename() {
        return this.typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getTypecode() {
        return this.typecode;
    }

    public void setTypecode(String typecode) {
        this.typecode = typecode;
    }

    public List<DictionaryType> getTSTypes() {
        return this.TSTypes;
    }

    public void setTSTypes(List<DictionaryType> TSTypes) {
        this.TSTypes = TSTypes;
    }

    public Long getTypeid() {
        return typeid;
    }

    public void setTypeid(Long typeid) {
        this.typeid = typeid;
    }

    public Long getTypegroupid() {
        return typegroupid;
    }

    public void setTypegroupid(Long typegroupid) {
        this.typegroupid = typegroupid;
    }

}

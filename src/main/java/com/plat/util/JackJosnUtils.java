package com.plat.util;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.ObjectMapper;
public class JackJosnUtils {

	private static JsonGenerator jsonGenerator = null;

	private static ObjectMapper objectMapper = new ObjectMapper();

	public static String toJson(Object obj) {

		try {
//			objectMapper.registerModule(new HibernateModule());
			return objectMapper.writeValueAsString(obj);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "{}";
	}
}

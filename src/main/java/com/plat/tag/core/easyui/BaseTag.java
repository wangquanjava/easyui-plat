package com.plat.tag.core.easyui;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.plat.tag.core.utils.oConvertUtils;

public class BaseTag extends TagSupport {
    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    protected String type = "default";// 加载类型

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int doStartTag() throws JspException {
        return EVAL_PAGE;
    }

    @Override
    public int doEndTag() throws JspException {
        try {
            JspWriter out = this.pageContext.getOut();
            StringBuffer sb = new StringBuffer();

            String types[] = type.split(",");
            Set<String> typeSet = new HashSet<String>();

            for (String s : types) {
                typeSet.add(s);
            }
            if (oConvertUtils.isIn("jq", typeSet)) {
                sb.append("<script type=\"text/javascript\" src=\"plug-in/jquery/jquery-1.8.3.min.js\"></script>");
                sb.append("<script type=\"text/javascript\" src=\"tools/jquery.cookie.js\"></script>");
            }
            if (oConvertUtils.isIn("easyui", typeSet)) {
                sb.append("<script type=\"text/javascript\" src=\"plug-in/jquery/jquery-1.8.3.min.js\"></script>");
                sb.append("<script type=\"text/javascript\" src=\"tools/dataformat.js\"></script>");
                sb.append("<link rel=\"stylesheet\" id=\"easyuiTheme\" href=\"plug-in/easy-ui/themes/pepper-grinder/easyui.css\" type=\"text/css\"></link>");
                sb.append("<link rel=\"stylesheet\" href=\"plug-in/easy-ui/themes/icon.css\" type=\"text/css\"></link>");
                sb.append("<link rel=\"stylesheet\" type=\"text/css\" href=\"plug-in/accordion/css/accordion.css\">");
                sb.append("<script type=\"text/javascript\" src=\"plug-in/easy-ui/jquery.easyui.min.js\"></script>");
                sb.append("<script type=\"text/javascript\" src=\"plug-in/easy-ui/locale/easyui-lang-zh_CN.js\"></script>");
                sb.append("<script type=\"text/javascript\" src=\"tools/syUtil.js\"></script>");
            }
            if (oConvertUtils.isIn("DatePicker", typeSet)) {
                sb.append("<script type=\"text/javascript\" src=\"plug-in/My97DatePicker/WdatePicker.js\"></script>");
            }
            if (oConvertUtils.isIn("jqueryui", typeSet)) {
                sb.append("<link rel=\"stylesheet\" href=\"plug-in/jquery-ui/css/ui-lightness/jquery-ui-1.9.2.custom.min.css\" type=\"text/css\"></link>");
                sb.append("<script type=\"text/javascript\" src=\"plug-in/jquery-ui/js/jquery-ui-1.9.2.custom.min.js\"></script>");
            }
            if (oConvertUtils.isIn("prohibit", typeSet)) {
                sb.append("<script type=\"text/javascript\" src=\"plug-in/tools/prohibitutil.js\"></script>");
            }

            if (oConvertUtils.isIn("tools", typeSet)) {
                //----begin -----Author:周俊峰   ---日期：2013-8-25----for：添加用户自定义样式common.css-----
                sb.append("<link rel=\"stylesheet\" href=\"tools/css/common.css\" type=\"text/css\"></link>");
                //----end -----Author:周俊峰    ---日期：2013-8-25----for：添加用户自定义样式common.css-----
                sb.append("<script type=\"text/javascript\" src=\"plug-in/jquery/json2.js\"></script>");
                sb.append("<script type=\"text/javascript\" src=\"plug-in/lhgDialog/lhgdialog.min.js?skin=iblue\"></script>");
                sb.append("<script type=\"text/javascript\" src=\"tools/crud.js\"></script>");
                sb.append("<script type=\"text/javascript\" src=\"tools/workflow.js\"></script>");
                sb.append("<script type=\"text/javascript\" src=\"tools/changeEasyuiTheme.js\"></script>");
            }
            if (oConvertUtils.isIn("toptip", typeSet)) {
                sb.append("<link rel=\"stylesheet\" href=\"plug-in/toptip/css/css.css\" type=\"text/css\"></link>");
                sb.append("<script type=\"text/javascript\" src=\"plug-in/toptip/manhua_msgTips.js\"></script>");
            }
            out.print(sb.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return EVAL_PAGE;
    }

}

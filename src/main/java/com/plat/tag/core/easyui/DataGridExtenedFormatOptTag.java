package com.plat.tag.core.easyui;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * 字段的format扩展,通过自定义的function实现
 * @author wench
 * 
 */
public class DataGridExtenedFormatOptTag extends TagSupport {

	protected String title;

	private String exp;// 判断链接是否显示的表达式

	private String funname;// 自定义函数名称

	private String operationCode;// 按钮的操作Code

	public int doStartTag() throws JspTagException {
		return EVAL_PAGE;
	}

	public int doEndTag() throws JspTagException {
		Tag t = findAncestorWithClass(this, DataGridTag.class);
		DataGridTag parent = (DataGridTag) t;
		parent.setExtenedFormatUrl(title, exp, funname, operationCode);
		return EVAL_PAGE;
	}

	public void setFunname(String funname) {
		this.funname = funname;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	}

}

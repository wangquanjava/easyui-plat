package com.plat.tag.core.vo;

/**
 * 列表操作类型
 */
public enum OptTypeDirection {
	Deff, Del, Fun, OpenWin, Confirm, ToolBar, OpenTab, ExtenedFormat

}

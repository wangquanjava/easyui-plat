package com.plat.tag.core.json;

public abstract class AjaxUtils {

    protected abstract Object busCall() throws Exception;

    public AjaxJson excuteJson(String successMsg, String errorMsg) {
        AjaxJson json = new AjaxJson();
        try {
            busCall();
            json.setMsg(successMsg);
            return json;
        } catch (Exception e) {
            errorMsg += ",错误描述:[" + e.getMessage() + "]";
            e.printStackTrace();
        }
        json.setSuccess(false);
        json.setMsg(errorMsg);
        return json;
    }
}

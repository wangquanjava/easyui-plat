package com.plat.dao;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;

import com.plat.core.hibernate.qbc.CriteriaQuery;
import com.plat.support.ModelSetup;
import com.plat.support.Page;
import com.plat.tag.core.easyui.ComboTreeModel;
import com.plat.tag.core.easyui.TreeGridModel;
import com.plat.tag.core.json.ComboTree;
import com.plat.tag.core.json.DataGridReturn;
import com.plat.tag.core.json.TreeGrid;

public interface IBaseDao<T> {

    /**
     * <br>
     * <b>功能：</b>获取单个对象<br>
     * <b>作者：</b>wench<br>
     * <b>日期：</b> 2012-7-29 <br>
     * @param entityClass
     * @param id
     * @return
     */
    public T get(Serializable id);

    /**
     * <br>
     * <b>功能：</b>获取所有对象<br>
     * <b>作者：</b>wench<br>
     * <b>日期：</b> 2012-7-29 <br>
     * @param entityClass
     * @return
     */
    public List<T> getAll();

    /**
     * 保存对象.
     */
    public void save(Object o);

    /**
     * 删除对象.
     */
    public void remove(Object o);

    /**
     * 根据ID删除对象.
     */
    public void removeById(Serializable id);

    public Query createQuery(String hql, Object... values);

    public Criteria createCriteria(Criterion... criterions);

    public Criteria createCriteria(String orderBy, boolean isAsc, Criterion... criterions);

    public List<T> find(String hql, Object... values);

    public List<T> findByValue(String hql, Object... values);

    public List<T> findBy(String propertyName, Object value);

    /**
     * 根据属性名和属性值查询对象,带排序参数.
     */
    public List<T> findBy(String propertyName, Object value, String orderBy, boolean isAsc);

    public T findUniqueBy(String propertyName, Object value);

    /**
     * 分页查询函数，使用hql.
     * @param pageNo
     *            页号,从1开始.
     */
    public Page pagedQuery(String hql, int pageNo, int pageSize, Object... values);

    /**
     * 统计条数
     * @param criteria
     * @return
     */
    public Integer getCount(Criteria criteria);

    /**
     * 分页查询函数，使用已设好查询条件与排序的<code>Criteria</code>.
     * @param pageNo
     *            页号,从1开始.
     * @return 含总记录数和当前页数据的Page对象.
     */
    public Page pagedQuery(Criteria criteria, int pageNo, int pageSize);

    /**
     * 分页查询函数，根据entityClass和查询条件参数创建默认的<code>Criteria</code>.
     * @param pageNo
     *            页号,从1开始.
     * @return 含总记录数和当前页数据的Page对象.
     */
    public Page pagedQuery(int pageNo, int pageSize, Criterion... criterions);

    /**
     * 分页查询函数，根据entityClass和查询条件参数,排序参数创建默认的<code>Criteria</code>.
     * @param pageNo
     *            页号,从1开始.
     * @return 含总记录数和当前页数据的Page对象.
     */
    public Page pagedQuery(int pageNo, int pageSize, String orderBy, boolean isAsc, Criterion... criterions);

    /**
     * 判断对象某些属性的值在数据库中是否唯一.
     * @param uniquePropertyNames
     *            在POJO里不能重复的属性列表,以逗号分割 如"name,loginid,password"
     */
    public boolean isUnique(Object entity, String uniquePropertyNames);

    /**
     * 取得对象的主键值,辅助函数.
     */
    public Serializable getId(Class entityClass, Object entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException;

    /**
     * 取得对象的主键名,辅助函数.
     */
    public String getIdName(Class clazz);

    /**
     * 得到物理字段名
     * @param <T>
     * @param entityClass
     * @param propertyName
     * @return
     */
    public String getColumnName(Class<T> entityClass, String propertyName);

    public Page pagedQuery(ModelSetup modelSetup, int pageNo, int pageSize);

    DataGridReturn getDataGridReturn(CriteriaQuery cq, boolean b);

    public void updateMerge(Object o);

    public List<ComboTree> comboTree(List all, ComboTreeModel comboTreeModel, List in);

    public List<TreeGrid> treegrid(List all, TreeGridModel treeGridModel);
}
